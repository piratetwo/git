      program bbf

      integer atype,n,iter,flag,utype,nl,nf,itermax
      double precision f,gnorm,rho,tol,gamma

C     LOCAL ARRAYS
      double precision x(10000),xnew(10000)

C     Dimension
      
      n = 5
      itermax = 1000
      tol = 10.0d0 ** (-6)
      utype = 2
      rho = 2.0d0
      gamma = 0.2
      do atype = 0,1
          call inipoint(n,x)
c          call bb(n,x,f,gnorm,iter,atype,itermax,flag,nf,nl,tol)
c          call mtbb(n,x,f,gnorm,iter,gamma,atype,utype,
c     +                        itermax,flag,nf,nl,tol)
c          call sbb(n,x,f,gnorm,iter,atype,utype,itermax,rho,
c     +            flag,nf,nl,tol)
          call gbb(n,x,f,gnorm,iter,atype,itermax,flag,nf,nl,tol)
          write(*,100) flag,iter,nf,f,gnorm
      end do
      
      do atype = 0,1
          call inipoint(n,x)
c          call bb(n,x,f,gnorm,iter,atype,itermax,flag,nf,nl,tol)
c          call mbb(n,x,f,gnorm,iter,atype,utype,itermax,rho,
c     +            flag,nf,nl,tol)
          call gmtbb(n,x,f,gnorm,iter,gamma,atype,utype,
     +                        itermax,flag,nf,nl,tol)
c          call gbbn(n,x,f,gnorm,iter,atype,itermax,flag,nf,nl,tol)
          write(*,100) flag,iter,nf,f,gnorm
      end do
      
      
      stop

C     NON-EXECUTABLE STATEMENTS

 100  format(/'WHETHER ALGORITHM CONVERGE              = ',8x,i6,
     +       /'NUMBER OF ITERATIONS                    = ',8X,I6,
     +       /'NUMBER OF function computations         = ',8X,I6,
     +       /'OBJECTIVE FUNCTION VALUE                = ',1PD14.7,
     +       /'GRADIENT EUCLIDIAN NORM                 = ',1PD14.7)
      end
 
      
      subroutine inipoint(n,x)

C     SCALAR ARGUMENTS
      integer n

C     ARRAY ARGUMENTS
      double precision x(n)

C     LOCAL SCALARS
      integer i

      do i = 1,n
          x(i) = 1.0d0
      end do

      end

c     ******************************************************************
c     ******************************************************************

      subroutine evalfg(n,x,f,g)

      integer n
      double precision f

C     ARRAY ARGUMENTS
      double precision x(n),g(n)

C     LOCAL SCALARS
      integer i

      f = 0.0d0
      do i = 1,n
          f    = f + ( 1.0d0 + mod(i,10) ) * x(i) ** 2
          g(i) = ( 1.0d0 + mod(i,10) ) * 2.0d0 * x(i)
      end do

      end