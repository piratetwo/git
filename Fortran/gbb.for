      subroutine gbb(n,x,f,gnorm,iter,atype,itermax,flag,nf,nl,tol)
c     Global convergent Barzilai and Borwein method
c     Raydan - 1997 - SIAM J. Optim. 7(1) : 26 - 33
      implicit none
      double precision f,gnorm,fnew,lam
      double precision x(n),g(n),xnew(n),d(n),y(n),gnew(n)
      integer iter,nl,nf,i,M,n,itermax,flag,atype
      
      double precision maxff(10)
      double precision gamma,eps1,eps2,sigma1,sigma2,alpha,maxf
      double precision gtg,gtd,ytg,yty
      double precision one,zero,tol,epsi1,epsi2
      data one,zero /1.0d0,0.0d0/
  
c     Initial parameters in the GBB method  
      epsi1 = 10.0d0 ** (-5)
      epsi2 = 10.0d0 ** 5
      gamma = 10.0d0 ** (-4)
      eps1 = 10.0d0 ** (-10)
      eps2 = 10.0d0 ** 10
      sigma1 = 0.1d0
      sigma2 = 0.5d0
      alpha = one
      M = 10
      
      do i = 1,M
          maxff(i) = zero
      end do

c     Initial Variables
      iter = 0
      nf = 0
      nl = 0
      alpha = one
      flag = 0
      
      call evalfg(n,x,f,g)
      nf = nf + 1
      maxff(1) = f
      maxf = maxval(maxff)

10    continue
      gtg = zero
      gtd = zero
      do i=1,n
          d(i) = - g(i)
          gtg = gtg + g(i)**2
      end do
      
      gtd = - gtg
      gnorm = dsqrt(gtg)
      
c     check the stopping rule
      if (gnorm .le. tol * (one + dabs(f))) goto 999
      
      if (iter .gt. itermax) go to 998
      iter = iter + 1

c     initial alpha if value of alpha is too small or large
      if ((alpha .le. eps1) .or. (alpha .ge. eps2)) then
          if (gnorm .gt. 1) then
              alpha = 1
          else if (gnorm .lt. epsi1) then
              alpha = epsi2
          else
              alpha = one / gnorm
          end if
      end if
      
      lam = one / alpha
      
c     nonmotone line search 
      call inter_quar(n,x,maxf,d,gtd,sigma1,sigma2,xnew,fnew,
     +                      gnew,nf,nl,lam,gamma)

c      write(*,*)xnew,fnew,lam
c     update maxff and maxf

      if (iter .lt. M) then
          maxff(iter + 1) = fnew       
      else
          do i = 1,M-1
              maxff(i) = maxff(i+1)
          end do
          maxff(M) = fnew
      end if
      
      maxf = maxval(maxff)
      
c     compute new stepsize  
      ytg = zero
      yty = zero
      do i = 1,n
          x(i) = xnew(i)
          y(i) = gnew(i) - g(i)
          yty = yty + y(i) * y(i)
          ytg = ytg + y(i) * g(i)
          g(i) = gnew(i)
      end do
      if (atype .eq. 1) then
          alpha = - ytg / gtg / lam
      else
          alpha = - yty / ytg / lam
      end if
      
      f = fnew
      
      goto 10

998   continue
      flag = 1

999   continue
      
      return
      end

      subroutine gbbn(n,x,f,gnorm,iter,atype,itermax,flag,nf,nl,tol)
c     Global convergent Barzilai and Borwein method
c     Raydan - 1997 - SIAM J. Optim. 7(1) : 26 - 33
c     but change the nonmotone line search to Hager and Zhang's
      implicit none
      double precision f,gnorm,fnew,lam
      double precision x(n),g(n),xnew(n),d(n),y(n),gnew(n)
      integer iter,nl,nf,i,n,itermax,flag,atype
      
      double precision c,q,cold,qold,eta
      double precision gamma,eps1,eps2,sigma1,sigma2,alpha
      double precision gtg,gtd,ytg,yty
      
      double precision one,zero,tol,epsi1,epsi2
      data one,zero /1.0d0,0.0d0/
  
c     Initial parameters in the GBB method  
      epsi1 = 10.0d0 ** (-5)
      epsi2 = 10.0d0 ** 5
      gamma = 10.0d0 ** (-4)
      eps1 = 10.0d0 ** (-10)
      eps2 = 10.0d0 ** 10
      sigma1 = 0.1d0
      sigma2 = 0.5d0
      alpha = one

c     Initial Variables
      iter = 0
      nl = 0
      nf = 0
      alpha = one
      flag = 0
      
      call evalfg(n,x,f,g)
      nf = nf + 1
      c = f
      q = zero
      cold = c
      qold = q

10    continue
      gtg = zero
      gtd = zero
      do i=1,n
          d(i) = - g(i)
          gtg = gtg + g(i)**2
      end do
      
      gtd = - gtg
      gnorm = dsqrt(gtg)
      
c     check the stopping rule
      if (gnorm .le. tol * (one + dabs(f))) goto 999
      
      if (iter .gt. itermax) go to 998
      iter = iter + 1

c     initial alpha if value of alpha is too small or large
      if ((alpha .le. eps1) .or. (alpha .ge. eps2)) then
          if (gnorm .gt. 1) then
              alpha = 1
          else if (gnorm .lt. epsi1) then
              alpha = epsi2
          else
              alpha = one / gnorm
          end if
      end if
      
      lam = one / alpha 
      
c     nonmotone line search 
      call inter_quar(n,x,c,d,gtd,sigma1,sigma2,xnew,fnew,
     +                      gnew,nf,nl,lam,gamma)
     
c     compute new stepsize
      ytg = zero
      yty = zero
      do i = 1,n
          x(i) = xnew(i)
          y(i) = gnew(i) - g(i)
          yty = yty + y(i) * y(i)
          ytg = ytg + y(i) * g(i)
          g(i) = gnew(i)
      end do
      if (atype .eq. 1) then
          alpha = - ytg / gtg / lam
      else
          alpha = - yty / ytg / lam
      end if
      
      f = fnew
      
c     update c and q
      q = eta * qold + 1
      c = (eta * qold * cold + f) / q
      
      goto 10

998   continue
      flag = 1

999   continue
      
      return
      end

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c	The modified Barzilai-Borwein method based on modified secant equations
c	
c	References:
c	Biglari, F., Solimanpur, M.: 
c         Scaling on the Spectral Gradient Method. 
c		J Optim Theory Appl. 158, 626每635 (2013).
c	Xiao, Y., Wang, Q., Wang, D.: 
c         Notes on the Dai每Yuan每Yuan modified spectral gradient method. 
c         J. Comput. Appl. Math. 234 , 2986 每2992 (2010)
c     Zhang, H., Hager, W.W.: 
c         A Nonmonotone Line Search Technique and Its Application to
c               Unconstrained Optimization. 
c         SIAM J. Optim. 14, 1043每1056 (2004).
c      
c               sts          sty
c     alpha =  ------   or -------
c               sty          yty
c      
c                       rho * theta
c     where   y = y + --------------  u
c                          stu
c
c             u is chosen such that stu is not equal to zero
c 
c             theta = 2 * (fk - fk+1) + s^T(gk + gk+1)
c
c             rho is a parameter in the interval [-1,3]  
c      
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc      
      subroutine sbb(n,x,f,gnorm,iter,atype,utype,itermax,
     +                    rho,flag,nf,nl,tol)
c      
c     atype   = the type of stepsize alpha
c             = 1, alpha = sts / sty
c             = 2, alpha = sty / yty

c     utype   = the choice of u in new y
c             = 1, u = sk    
c             = 2, u = yk
c      
c     flag    = whether the iteration converges
c             = 1,   no
c             = 0,   yes

      implicit none
      double precision f,gnorm
      integer n,iter,flag,i,atype,nf,itermax,utype,nl
      double precision c,q,cold,qold

      double precision gtd,gty,gtg,gtgn,fnew,alpha,yty,
     +        theta,rho,beta,gntgn,temp,alphaprev
      double precision sigma1,sigma2,gamma,lammax,lammin,eta
      double precision x(n),d(n),gnew(n),g(n),y(n),xnew(n)
      
c   initial some constants
      double precision one,zero,tol
      data one,zero /1.0d0,0.0d0/
      
      sigma1 = 0.1d0
      sigma2 = 0.5d0
      gamma = 10.0d0 ** (-4)
      lammax = 10.0d0 ** (30)
      lammin = 10.0d0 ** (-30)
      eta = 0.8d0

c     Initial Variables
      iter = 0
      nl = 0
      nf = 0
      flag = 0

c   evaluate initial function and gradient values
    
      call evalfg(n,x,f,g)
      nf = nf + 1
      
      c = f
      q = one
      cold = c
      qold = q
      alpha = one
      
      gtd = zero
      gtg = zero
      do i = 1,n
          d(i) = - g(i)
          gtg = gtg + g(i) * g(i)
      end do
      gtd = - gtg
      gnorm = dsqrt(gtg)
      
10    continue    
c     check the stopping criterion      
      if (gnorm .le. tol * (one + abs(f))) go to 999
c      if (ginf .le. eps) goto 99

      if (iter .gt. itermax) goto 998
      
      iter = iter + 1

      call inter_quar(n,x,c,d,gtd,sigma1,sigma2,xnew,fnew,
     +                      gnew,nf,nl,alpha,gamma)
      
c     store the previous step of alpha
      alphaprev = alpha
      
      gty = zero
      yty = zero
      gtg = zero
      gtgn = zero
      gntgn = zero
      do i = 1,n
          x(i) = xnew(i)
          d(i) = - gnew(i)
          gtg = gtg + g(i) * g(i)
          gtgn = gtgn + g(i) * gnew(i)
          gntgn = gntgn + gnew(i) * gnew(i)
          g(i) = gnew(i)
      end do
      gtd = - gntgn
      gnorm = dsqrt(gntgn)
      
c     evaluate stepsize
      theta = 2 * (fnew - f) / alphaprev + gtg + gtgn
      gty = gtgn - gtg
      if ( atype .eq. 1) then
          alpha = - alphaprev * gtg / (gty + rho * theta)
      else
          yty = gtg + gntgn - 2 * gtgn
c         uk = sk
          if (utype .eq. 1) then
              temp = rho * theta
              temp = yty + 2 * temp * gty / gtg + temp * temp / gtg
              alpha = - alphaprev * gty / temp
          else
c         uk = yk
              beta = 1 + rho * theta / gty
              alpha = - alphaprev * gty / beta / yty
          end if
      end if
      
      f = fnew
               
c     truncate the stepsize      
c      if (alpha .lt. zero) then
c          alpha = - alphaprev * gtg / gty
c      end if
      
      alpha = max(lammin,alpha)
      alpha = min(lammax,alpha)
c     update q and c
      q = eta * qold + 1
      c = (eta * qold * cold + f) / q
      qold = q
      cold = c
      
      goto 10

998   continue
      flag = 1

999   continue
      
      return
      end
      
      subroutine inter_quar(n,xold,fold,pk,gtp,lower,upper,xnew,fnew,
     +                        gnew,nf,nl,lam,alpha)
c     quadratic interpolation backtracking linear searching
c     Numerical Methods for Unconstrained Optimization and
c     Nonlinear Eauations J. E. Dennis, Jr. Robert B. Schnabel
      
      implicit none
      double precision lam,fold,fnew,gtp
      double precision xold(n),xnew(n),pk(n),gnew(n)
      double precision laml,lamu,temp1,temp2,lower,upper
      integer i,n,nl,nf
      double precision one,zero,alpha,gtpint
      data one,zero /1.0d0,0.0d0/

      gtpint = alpha * gtp

10    continue
      laml = lam * lower
      lamu = lam * upper
            
      do i=1,n
          xnew(i) = xold(i) + lam * pk(i)
      end do
      
      call evalfg(n,xnew,fnew,gnew)
      nf = nf + 1
      
      if (fnew .le. fold + lam * gtpint) go to 999
      nl = nl + 1
      
      temp1 = - gtp
      temp2 = 2 * (fnew - fold - gtp)
      lam = temp1 / temp2
      lam = max(lam,laml)
      lam = min(lam,lamu)
      goto 10
      
999   continue
      
      return
      end