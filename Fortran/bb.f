      subroutine bb(n,x,fnew,gnorm,iter,atype,itermax,
     +                     flag,nf,nl,tol)
c     flag    = whether the iteration converges
c                 1   no
c                 0   yes

c	The Barzilai–Borwein method or the scaled spectral gradient method
c	
c	References:
c	Raydan, M.: The Barzilai and Borwein gradient method for the large 
c		scale unconstrained minimization problem. SIAM J. Optim. 7, 26–33 (1997)
c	Barzilai, J., Borwein, J.M.: Two point stepsize gradient methods.
c		IMA J. Numer. Anal.8, 141–148 (1988)

      implicit none
      double precision f,gnorm,tol
      integer n,iter,flag,i,atype,nf,itermax,nl
 
      double precision dtg,ytg,gtg,ginf,gtgn,fnew,alpha,yty
      double precision x(n),d(n),gnew(n),g(n),y(n),xnew(n)
      
c   initial some constants
      double precision one,zero
      data one,zero /1.0d0,0.0d0/

c   initial the iterative parameters
      iter = 0
      flag = 0
      nf = 0
      
c   evaluate initial function and gradient values
    
      call evalfg(n,x,f,g)
      nf = nf +1
      
      dtg = zero
      gtg = zero
      do i = 1,n
          d(i) = - g(i)
          gtg = gtg + g(i) * g(i)
      end do
      dtg = - gtg
      gnorm = dsqrt(gtg)
      
      alpha = one / gnorm
      
c     the first stepsize satisfied Armijo rule      
      call back(n,x,d,f,dtg,alpha,xnew,fnew,gnew,nf,nl)
           
10    continue
      ytg = zero
      yty = zero
      gtgn = zero
      ginf = zero
      do i = 1,n
          y(i) = gnew(i) - g(i)
          ytg = ytg + y(i) * g(i)
          yty = yty + y(i) * y(i)
          gtgn = gtgn + gnew(i) * gnew(i)
          ginf = max1(ginf,dabs(gnew(i)))
      end do
      
      gnorm = dsqrt(gtgn)
      
c     check the stopping criteria 
      if (gnorm .le. tol * (one + dabs(fnew))) go to 99
c      if (ginf .le. eps) goto 99
      if (iter .gt. itermax) goto 999
      iter = iter + 1
      
c     evaluate stepsize 
      if ( atype .eq. 1) then
          alpha = - alpha * gtg / ytg
      else
          alpha = - alpha * ytg / yty
      end if
      
      gtg = gtgn
      
c     update the iterate point      
      do i = 1,n
          xnew(i) = xnew(i) - alpha * gnew(i)
          g(i) = gnew(i)
      end do
      
      call evalfg(n,xnew,fnew,gnew)
      nf = nf + 1
      goto 10
      
999   continue
      flag = 1

99    continue
      
      return
      end
     
      subroutine back(n,x,d,fx,dtg,alpha,xnew,fnew,gnew,nf,nl)
c     backward line search for Armijo rule
      implicit none
      double precision x(n),d(n),xnew(n),gnew(n)
      double precision fx,fnew,dtg,alpha,dtgint
      integer nf,nl,i,n

      double precision sigma,s
      data sigma,s /0.0001d0,0.8d0/
      
      dtgint = dtg * sigma
      
c     main loop
10    continue
      do i=1,n
        xnew(i) = x(i) + alpha * d(i) 
      end do

      call evalfg(n,xnew,fnew,gnew)
      nf = nf + 1
      
      if(fnew .le. (fx + alpha * dtgint)) go to 99
      nl = nl + 1
      alpha = alpha * s
      
      go to 10
                    
99    continue 
 
      return
      end
      
      
      subroutine mbb(n,x,fnew,gnorm,iter,atype,utype,itermax,rho,
     +                       flag,nf,nl,tol)
c     modified BB gradient method for unconstrained optimization
      
      implicit none
      double precision f,gnorm,tol,rho
      integer iter,atype,flag,nl,nf,itermax,n,utype,i
      
      double precision fnew,alpha,alphaprev,beta
      double precision x(n),xnew(n),g(n),gnew(n),d(n),y(n)
      double precision gtg,gtd,gtgn,gty,yty,gntgn,temp,theta
      
      double precision zero,one
      data zero,one /0.0d0,1.0d0/
      
      flag = 0
      nl = 0
      nf = 0
      iter = 0
      
      call evalfg(n,x,f,g)
      nf = nf + 1
      
      gtd = zero
      gtg = zero
      do i=1,n
          d(i) = - g(i)
          gtg = gtg + g(i) * g(i)
      end do
      gtd = - gtg
      gnorm = dsqrt(gtg)
      
      alpha = one / gnorm
c     the first stepsize satisfied Armijo rule      
      call back(n,x,d,f,gtd,alpha,xnew,fnew,gnew,nf,nl)

10    continue
c     store the previous step of alpha
      alphaprev = alpha

      gty = zero
      yty = zero
      gtgn = zero
      gntgn = zero
      do i = 1,n
          gtgn = gtgn + g(i) * gnew(i)
          gntgn = gntgn + gnew(i) * gnew(i)
      end do
      gnorm = dsqrt(gntgn)
      
c     check stopping rule
      if (gnorm .le. tol * (one + dabs(fnew))) goto 99
      if (iter .gt. itermax) goto 999
      iter = iter + 1
      
c     evaluate new stepsize
      theta = 2 * (fnew - f) / alphaprev + gtg + gtgn
      gty = gtgn - gtg
      if ( atype .eq. 1) then
          alpha = - alphaprev * gtg / (gty + rho * theta)
      else
          yty = gtg + gntgn - 2 * gtgn
c         uk = sk
          if (utype .eq. 1) then
              temp = rho * theta
              temp = yty + 2 * temp * gty / gtg + temp * temp / gtg
              alpha = - alphaprev * gty / temp
          else
c         uk = yk
              beta = 1 + rho * theta / gty
              alpha = - alphaprev * gty / beta / yty
          end if
      end if
      
      f = fnew
      gtg = gntgn
      
c     update the iterate point 
      do i = 1,n
          xnew(i) = xnew(i) - alpha * gnew(i)
          g(i) = gnew(i)
      end do
      
      call evalfg(n,xnew,fnew,gnew)
      nf = nf + 1
      goto 10
      

999   continue
      flag = 1

99    continue

      return
      end
      
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     modified BB method based on multi-step quasi-Newton equation
c
c               rk'wk         wk'wk
c     alpha = ---------  or ----------
c               rk'rk         rk'wk
c
c     where   rk = sk - gamma * sk-1 
c                = alphak (gk - gamma * alphak-1 / alphak * gk)
c             wk = yk - gamma * yk-1
c     
c     References:
c     J.A. Ford, Y. Narushima, and H. Yabe. 
c         Multi-step nonlinear conjugate gradient
c             methods for unconstrained minimization. 
c         Comput. Optimization and Applications, 40(2):191–216, 2008.
c     F. Modarres, M. Hassan, and W. Leong. 
c         Multi-steps symmetric rank-one update for unconstrained
c             optimization.
c         World Applied Sciences Journal, 7(5):610–615, 2009.
c      
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc     
      subroutine mtbb(n,x,f,gnorm,iter,gamma,atype,utype,
     +                        itermax,flag,nf,nl,tol)
      implicit none
      double precision f,gnorm,gamma,tol
      integer iter,atype,utype,itermax,flag,nf,i,n,nl
      
      double precision x(n),g(n),xnew(n),g1(n),y(n),d(n),
     +                    y1(n),y2(n),g2(n),g0(n)
      double precision gtg,gtd,ytd,yty,ytg,alpha,fnew,alpha1,alpha2,
     +                 alpha12
     
      double precision one,zero
      data one,zero /1.0d0,0.0d0/
      
c     initial some parameters of the method
      flag = 0
      iter = 0
      nf = 0
      nl = 0
      
      call evalfg(n,x,f,g0)
      nf = nf + 1
      
      gtd = zero
      gtg = zero
      do i = 1,n
          d(i) = - g0(i)
          gtg = gtg + g0(i) * g0(i)
      end do
      gtd = - gtg
      gnorm =  dsqrt(gtg)
      
      alpha = one / gnorm
      
      call back(n,x,d,f,gtd,alpha,xnew,fnew,g1,nf,nl)
      
      alpha1 = alpha
      
      ytg = zero
      yty = zero
      do i = 1,n
          y1(i) = g1(i) - g0(i)
          ytg = ytg + y1(i) * g0(i)
          yty = yty + y1(i) * y1(i)
      end do
      
      if (atype .eq. 1) then
          alpha2 = - alpha1 * gtg / ytg
      else
          alpha2 = - alpha1 * ytg / yty
      end if
      
      do i = 1,n
          xnew(i) = xnew(i) - alpha2 * g1(i)
      end do
      
      call evalfg(n,xnew,f,g2)
      nf = nf + 1

c     main loop
10    continue
c     compute new y and g
      do i = 1,n
          y2(i) = g2(i) - g1(i)
      end do
      
      alpha12 = alpha1 / alpha2
      
      yty = zero
      ytg = zero
      gtg = zero
      do i = 1,n
          y(i) = y2(i) - gamma * y1(i)
          g(i) = g1(i) - gamma * alpha12 * g0(i)
          gtg = gtg + g(i) * g(i)
          yty = yty + y(i) * y(i)
          ytg = ytg + y(i) * g(i)
          y1(i) = y2(i)
          g0(i) = g1(i)
          g1(i) = g2(i)
      end do
      
      if (atype .eq. 1) then
          alpha = - alpha2 * gtg / ytg
      else
          alpha = - alpha2 * ytg / yty
      end if
      
      alpha1 = alpha2
      alpha2 = alpha
      
      do i = 1,n
          xnew(i) = xnew(i) - alpha * g2(i)
      end do
      
      call evalfg(n,xnew,f,g2)
      nf = nf + 1
      
      gtg = zero
      do i = 1,n
          gtg = gtg + g2(i) * g2(i)
      end do
      gnorm = dsqrt(gtg)
      
c     check stopping rule
      if (gnorm .le. tol * (one + dabs(f))) goto 99
      if (iter .gt. itermax) goto 999
      
      iter = iter + 1
      
      goto 10
      
999   continue
      flag = 1
      
99    continue
      
      return
      end

      
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      
c     globally convergent multi-step BB gradient method
c     
c     References:
c     
c     J.A. Ford, Y. Narushima, and H. Yabe. 
c         Multi-step nonlinear conjugate gradient
c             methods for unconstrained minimization. 
c         Comput. Optimization and Applications, 40(2):191–216, 2008.
c     F. Modarres, M. Hassan, and W. Leong. 
c         Multi-steps symmetric rank-one update for unconstrained
c             optimization.
c         World Applied Sciences Journal, 7(5):610–615, 2009.
c
c     Zhang, H., Hager, W.W.: 
c         A Nonmonotone Line Search Technique and Its Application to
c               Unconstrained Optimization. 
c         SIAM J. Optim. 14, 1043–1056 (2004).
c
c     Raydan, M.:
c         Global convergent Barzilai and Borwein method
c         SIAM J. Optim. 7(1):26 - 33, 1997
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      
      subroutine gmtbb(n,x,f,gnorm,iter,gamma,atype,utype,
     +                    itermax,flag,nf,nl,tol)
      implicit none
      double precision f,gnorm,gamma
      integer n,iter,atype,utype,itermax,flag,nf,nl,i
      
      double precision g(n),xnew(n),g1(n),y(n),d(n),x(n),
     +                    y1(n),y2(n),g2(n),g0(n)
      double precision gtg,gtd,ytd,yty,ytg,alpha,fnew,alpha1,alpha2,
     +                 alpha12

      double precision lammin,lammax,sigma1,sigma2,eta
      double precision c,q,cold,qold
     
      double precision one,zero,tol
      data one,zero /1.0d0,0.0d0/
      
      sigma1 = 0.1d0
      sigma2 = 0.5d0
      gamma = 10.0d0 ** (-4)
      lammax = 10.0d0 ** (30)
      lammin = 10.0d0 ** (-30)
      eta = 0.8d0
           
c     initial some parameters of the method
      flag = 0
      iter = 0
      nf = 0
      nl = 0
      
      call evalfg(n,x,f,g0)
      nf = nf + 1
      
      gtd = zero
      gtg = zero
      do i = 1,n
          d(i) = - g0(i)
          gtg = gtg + g0(i) * g0(i)
      end do
      gtd = - gtg
      gnorm =  dsqrt(gtg)
      
      alpha = one / gnorm
      
      call back(n,x,d,f,gtd,alpha,xnew,fnew,g1,nf,nl)
      
      alpha1 = alpha
      
      ytg = zero
      yty = zero
      do i = 1,n
          y1(i) = g1(i) - g0(i)
          ytg = ytg + y1(i) * g0(i)
          yty = yty + y1(i) * y1(i)
      end do
      
      if (atype .eq. 1) then
          alpha2 = - alpha1 * gtg / ytg
      else
          alpha2 = - alpha1 * ytg / yty
      end if
      
      do i = 1,n
          xnew(i) = xnew(i) - alpha2 * g1(i)
      end do
      
      call evalfg(n,xnew,f,g2)
      nf = nf + 1

c     begin the process of global convergence     

      c = f
      q = one
      cold = f
      qold = one

c     main loop
10    continue
c     compute new y and g
      do i = 1,n
          y2(i) = g2(i) - g1(i)
      end do
      
      alpha12 = alpha1 / alpha2
      
      yty = zero
      ytg = zero
      gtg = zero
      do i = 1,n
          y(i) = y2(i) - gamma * y1(i)
          g(i) = g1(i) - gamma * alpha12 * g0(i)
          gtg = gtg + g(i) * g(i)
          yty = yty + y(i) * y(i)
          ytg = ytg + y(i) * g(i)
          y1(i) = y2(i)
          g0(i) = g1(i)
          g1(i) = g2(i)
      end do
      
      if (atype .eq. 1) then
          alpha = - alpha2 * gtg / ytg
      else
          alpha = - alpha2 * ytg / yty
      end if
      
      alpha1 = alpha2
      
      alpha = max(lammin,alpha)
      alpha = min(lammax,alpha)
      
c     update q and c
      q = eta * qold + 1
      c = (eta * qold * cold + f) / q
      qold = q
      cold = c
      
      gtd = zero
      do i = 1,n
          x(i) = xnew(i)
          d(i) = - g2(i)
          gtd = gtd + g2(i) * d(i)
      end do
                 
      call inter_quar(n,x,c,d,gtd,sigma1,sigma2,xnew,fnew,
     +                      g2,nf,nl,alpha,gamma)
      
      alpha2 = alpha
            
      gtg = zero
      do i = 1,n
          gtg = gtg + g2(i) * g2(i)
      end do
      gnorm = dsqrt(gtg)
      f = fnew
      
c     check stopping rule
      if (gnorm .le. tol * (one + dabs(f))) goto 99
      if (iter .gt. itermax) goto 999
      
      iter = iter + 1
      
      goto 10
      
999   continue
      flag = 1
      
99    continue
      
      return
      end