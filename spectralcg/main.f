      program main
c     main function
      
      integer atype,n,iter,flag,utype,nl,nf,ng,itermax,nexp,i
      integer*4 gh,gm,gs,gc, ght,gmt,gst,gct, timpexp
      double precision f,gnorm,rho,tol,gamma
      double precision x(10000)
      open(unit=4,file='sbb1.txt',status='unknown')
      write(4,*) "np  size flag   iter  nf   ng   nl        valf     
     +     valg     time  ta  tu"
      
      itermax = 5000
      tol = 10.0d0 ** (-6)
      rho = 1.0d0
      gamma = 0.2
      
      
      call inipoint(n,x,2)
      print *,x(1:n)
      
      do nexp = 1,2
          n = 10
      do i = 1,3
          n = n * 10
      do atype = 1,2
      do utype = 1,2
          call inipoint(n,x,nexp)
          call gettim(gh,gm,gs,gc)
          
c     call gbbn(n,x,f,gnorm,iter,atype,itermax,flag,nf,ng,nl,tol,nexp)
c          call mtbb(n,x,f,gnorm,iter,gamma,atype,utype,
c     +                        itermax,flag,nf,ng,nl,tol,nexp)
          call sbb(n,x,f,gnorm,iter,atype,utype,itermax,rho,
     +            flag,nf,ng,nl,tol,nexp)
c         call gbb(n,x,f,gnorm,iter,atype,itermax,flag,nf,ng,nl,tol,nexp)
          
          call gettim(ght,gmt,gst,gct)
          call exetim(gh,gm,gs,gc, ght,gmt,gst,gct)
          timpexp = ght*360000 + gmt*6000 + gst*100 + gct
          
c          write(4,100) nexp,flag,iter,nf,f,gnorm,timpexp
c          write(*,200) nexp,n,flag,iter,nf,nl,f,gnorm,timpexp,atype
          write(4,200) nexp,n,flag,iter,nf,ng,nl,f,gnorm,timpexp,
     +                    atype,utype
      end do
      end do
      end do
      end do
      
200   format(1x,i2, 1x,i5, 1x,i2, 1x,i5, 1x,i5, 1x,i5, 1x,i5, 
     +            1PD14.7, 1PD14.7, 4x,i6, 1x,i2, 1x,i2)
100   format(/'TESTING PROBLEM                         = ',8X,I6,
     +       /'WHETHER ALGORITHM CONVERGE              = ',8x,i6,
     +       /'NUMBER OF ITERATIONS                    = ',8X,I6,
     +       /'NUMBER OF FUNCTION COMPUTATIONS         = ',8X,I6,
     +       /'NUMBER OF GRADIENT COMPUTATIONS         = ',8X,I6,
     +       /'OBJECTIVE FUNCTION VALUE                = ',1PD14.7,
     +       /'GRADIENT EUCLIDIAN NORM                 = ',1PD14.7,
     +       /'COST TIME                               = ',8x,i6)
      end program main
         
      

c     timer function and testing problems
c     
c
*----------------------------------------------------------------
*
	subroutine exetim(tih,tim,tis,tic, tfh,tfm,tfs,tfc)
*
	  integer*4 tih,tim,tis,tic
	  integer*4 tfh,tfm,tfs,tfc
*
	  integer*4 ti,tf
	  integer*4 ch,cm,cs
	  data ch,cm,cs/360000,6000,100/
*
	  ti=tih*ch+tim*cm+tis*cs+tic
	  tf=tfh*ch+tfm*cm+tfs*cs+tfc
	  tf=tf-ti
	  tfh=tf/ch
	  tf=tf-tfh*ch
	  tfm=tf/cm
	  tf=tf-tfm*cm
	  tfs=tf/cs
	  tfc=tf-tfs*cs
*
	  return
	end
*-------------------------------------------------- End of EXETIM