      subroutine inipoint(n,x,nexp)

C     This subroutine computes the initial point

      real*8 x(n)
      
      go to ( 1, 2, 3, 4, 5, 6, 7, 8, 9,10,
     *       11,12,13,14,15,16,17,18,19,20,
     *       21,22,23,24,25,26,27,28,29,30,
     *       31,32,33,34,35,36,37,38,39,40,
     *       41,42,43,44,45,46,47,48,49,50,
     *       51,52,53,54,55,56,57,58,59,60,
     *       61,62,63,64,65,66,67,68,69,70,
     *       71) nexp

1     continue
c                             Freudenstein & Roth
        i=1
991     x(i)  =   0.5d0
        x(i+1)=  -2.d0
        i=i+2
        if(i.le.n) go to 991      
      return
      
2     continue
c                             Trigonometric        
      do i=1,n
        x(i) = 0.2d0 
      end do
      return
      
3     continue
c                             Rosenbrock 
        i=1
993     x(i)  =  -1.2d0
        x(i+1)=   1.d0
        i=i+2
        if(i.le.n) go to 993      
      return

4     continue
c                             White & Holst
        i=1
994     x(i)  =  -1.2d0
        x(i+1)=   1.d0
        i=i+2
        if(i.le.n) go to 994      
      return                

5     continue
c                             Beale
        i=1
995     x(i)  =   1.d0
        x(i+1)=   0.8d0
        i=i+2
        if(i.le.n) go to 995      
      return      
      
6     continue
c                             Penalty
      do i=1,n  
       x(i) = float(i)        
      end do   
      return
      
7     continue
c                             Perturbed Quadratic
      do i=1,n
        x(i) = 0.5d0 
      end do   
      return      
      
8     continue
c                             Raydan 1
      do i=1,n
        x(i) = 1.d0 
      end do   
      return     
      
9     continue
c                             Raydan 2
      do i=1,n
        x(i) = 1.d0 
      end do   
      return       

10    continue
c                             Diagonal 1
      do i=1,n
        x(i) = 1.d0/float(n) 
      end do   
      return                    
      
11    continue
c                             Diagonal 2
      do i=1,n
        x(i) = 1.d0/float(i) 
      end do   
      return                
      
12    continue
c                             Diagonal 3
      do i=1,n
        x(i) = 1.d0 
      end do   
      return     
      
13    continue
c                             Hager
      do i=1,n
        x(i) = 1.d0 
      end do   
      return     
      
14    continue
c                             Generalized Tridiagonal 1
      do i=1,n
        x(i) = 2.d0 
      end do   
      return     
      
15    continue
c                             Extended Tridiagonal 1
      do i=1,n
        x(i) = 2.d0 
      end do   
      return     
      
16    continue
c                             Extended Three Expo Terms
      do i=1,n
        x(i) = 0.1d0 
      end do   
      return      
      
17    continue                
c                             Generalized Tridiagonal 2
      do i=1,n
        x(i) = -1.d0 
      end do   
      return      
      
18    continue
c                             Diagonal 4
      do i=1,n
        x(i) = 1.d0 
      end do   
      return     
      
19    continue
c                             Diagonal 5
      do i=1,n
        x(i) = 1.1d0 
      end do   
      return    
      
20    continue
c                             Extended Himmelblau
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 
      
21    continue
c                             Generalized PSC1                           
        i=1
9921    x(i)  =  3.d0
        x(i+1)=  0.1d0
        i=i+2
        if(i.le.n) go to 9921      
      return            
      
22    continue
c                             Extended PSC1
        i=1
9922    x(i)  =  3.d0
        x(i+1)=  0.1d0
        i=i+2
        if(i.le.n) go to 9922       
      return
      
23    continue
c                             Extended Powell
        i=1
9923    x(i)  =  3.d0
        x(i+1)= -1.d0
        x(i+2)=  0.d0
        x(i+3)=  1.d0
        i=i+4
        if(i.le.n) go to 9923             
      return
      
24    continue
c                             Extended BD1
      do i=1,n
        x(i) = 0.101d0 
      end do   
      return      
      
25    continue
c                             Extended Maratos
        i=1
9925    x(i)  =  1.1d0
        x(i+1)=  0.1d0
        i=i+2
        if(i.le.n) go to 9925       
      return        
      
26    continue
c                             Extended Cliff
        i=1
9926    x(i)  =   0.d0
        x(i+1)=  -1.d0
        i=i+2
        if(i.le.n) go to 9926       
      return      
      
27    continue
c                             Quadratic Diagonal Perturbed
      do i=1,n
        x(i) = 0.5d0 
      end do   
      return      
      
28    continue
c                             Extended Wood
        i=1
9928    x(i)  =  -3.d0
        x(i+1)=  -1.d0
        x(i+2)=  -3.d0
        x(i+3)=  -1.d0
        i=i+4
        if(i.le.n) go to 9928       
      return       

29    continue
c                             Extended Hiebert
      do i=1,n
        x(i) = 0.0001d0 
      end do   
      return
      
30    continue
c                             Quadratic QF1
      do i=1,n
        x(i) = 1.d0 
      end do   
      return  
                  
31    continue      
c                             Extended Quadratic Penalty QP1
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 

32    continue
c                             Extended Quadratic Penalty QP2
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 
      
33    continue
c                             Quadratic QF2                                                    
      do i=1,n
        x(i) = 0.5d0 
      end do   
      return      
      
34    continue
c                             Extended EP1
      do i=1,n
        x(i) = 1.5d0 
      end do   
      return      
      
35    continue
c                             Extended Tridiagonal
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 
      
36    continue
c                             BDQRTIC (CUTE)
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 
      
37    continue
c                             TRIDIA (CUTE)
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 
      
38    continue
c                             ARWHEAD (CUTE)
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 
      
39    continue
c                             NONDIA (CUTE)
      do i=1,n
        x(i) = -1.d0 
      end do   
      return    
      
40    continue
c                             NONDQUAR (CUTE)
        i=1
9940    x(i)  =   1.d0
        x(i+1)=  -1.d0
        i=i+2
        if(i.le.n) go to 9940       
      return                                   

41    continue
c                             DQDRTIC (CUTE)
      do i=1,n
        x(i) = 3.d0 
      end do   
      return             

42    continue
c                             EG2 (CUTE)
      do i=1,n
        x(i) = 1.d0 
      end do   
      return 

43    continue
c                             DIXMAANA (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return 

44    continue
c                             DIXMAANB (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return
      
45    continue
c                             DIXMAANC (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return             

46    continue
c                             DIXMAANE (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return 
      
47    continue
c                             Partial Perturbed Quadratic
      do i=1,n
        x(i) = 0.5d0 
      end do   
      return                  

48    continue
c                             Broyden Tridiagonal 
      do i=1,n
        x(i) = -1.d0 
      end do   
      return

49    continue
c                             Almost Perturbed Quadratic
      do i=1,n
        x(i) = 0.5d0 
      end do   
      return 

      
50    continue
c                             Tridiagonal Perturbed Quadratic
      do i=1,n
        x(i) = 0.5d0 
      end do
      return

      
51    continue
c                             EDENSCH (CUTE)
      do i=1,n
        x(i) = 0.0d0 
      end do   
      return 



52    continue
c                             VARDIM (CUTE)
      do i=1,n
        x(i) = 1.d0 - float(i)/float(n) 
      end do   
      return


53    continue
c                             STAIRCASE S1
      do i=1,n
        x(i) = 1.0d0 
      end do   
      return      

      
54    continue
c                             LIARWHD (CUTE)
      do i=1,n
        x(i) = 4.d0 
      end do   
      return  

      
55    continue            
c                             DIAGONAL 6 
      do i=1,n
        x(i) = 1.d0
      end do
      return


56    continue
c                             DIXON3DQ (CUTE)
      do i=1,n
        x(i) = -1.d0 
      end do   
      return 

      
57    continue
c                             DIXMAANF (CUTE)     
      do i=1,n
        x(i) = 2.d0 
      end do   
      return 

58    continue
c                             DIXMAANG (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return 
      
59    continue
c                             DIXMAANH (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return 
      
60    continue
c                             DIXMAANI (CUTE)                 
      do i=1,n
        x(i) = 2.d0 
      end do   
      return 
      
61    continue
c                             DIXMAANJ (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return 
            
62    continue
c                             DIXMAANK (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return
      
63    continue
c                             DIXMAANL (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return
      
64    continue
c                             ENGVAL1 (CUTE)
      do i=1,n
        x(i) = 2.d0 
      end do   
      return
      
65    continue
c                             FLETCHCR (CUTE)
      do i=1,n
        x(i) = 0.d0 
      end do   
      return    
      
66    continue
c                             COSINE (CUTE)
      do i=1,n
        x(i) = 1.d0 
      end do   
      return                    

67    continue
c                             DENSCHNB (CUTE)
      do i=1,n
        x(i) = 1.d0 
      end do   
      return

      
68    continue
c                             DENSCHNF (CUTE)      
        i=1
9968    x(i)  = 2.d0
        x(i+1)= 0.d0
        i=i+2
        if(i.le.n) go to 9968
      return


69    continue
c                             SINQUAD  (CUTE)
      do i=1,n
        x(i) = 0.1d0 
      end do   
      return      


70    continue
c                             BIGGSB1  (CUTE)
      do i=1,n
        x(i) = 0.d0            
      end do
      return

            
71	  continue
c                            Partial Perturbed Quadratic PPQ2
      do i=1,n
        x(i) = 0.5d0            
      end do
      return 

      end 
c------------------------------------------------------ End INIPOINT
      
*                  ******  EVALFG  ******
*                  ======================


**************************************************************
*                               Date created:  October 4, 2004
*
*
*       TEST FUNCTIONS FOR UNCONSTRAINED OPTIMIZATION   
*       ==============================================
*
* Trace of modifications:
*
*   57 problems:  October 28, 2004
*   66 problems:  March 29, 2005
*   70 problems:  April 28, 2005
*   73 problems:  October 14, 2005
*
**************************************************************  
      subroutine evalf(n,x,f, nexp)
      real*8 x(n), f  
      
      real*8 t1,t2,t3,t4, c, d
      real*8 s, temp(11000), temp1, tsum, sum
      real*8 u(11000), v(11000), t(11000) 
      real*8 u1, v1, c1, c2
      real*8 alpha, beta, gamma, delta
                       
      integer k1, k2, k3, k4      
                             
*      
      go to ( 1, 2, 3, 4, 5, 6, 7, 8, 9,10,
     *       11,12,13,14,15,16,17,18,19,20,
     *       21,22,23,24,25,26,27,28,29,30,
     *       31,32,33,34,35,36,37,38,39,40,
     *       41,42,43,44,45,46,47,48,49,50,
     *       51,52,53,54,55,56,57,58,59,60,
     *       61,62,63,64,65,66,67,68,69,70,
     *       71) nexp  
     
     

cF1                       Extended Freudenstein & Roth
*
*                      Initial Point: [0.5, -2, ...,0.5, -2].
*

1     continue
      f = 0.d0    
      
      do i=1,n/2 
        t1=-13.d0+x(2*i-1)+5.d0*x(2*i)*x(2*i)-x(2*i)**3-2.d0*x(2*i) 
        t2=-29.d0+x(2*i-1)+x(2*i)**3+x(2*i)**2-14.d0*x(2*i)   
        
        f = f + t1*t1 + t2*t2    
      end do   
      
      return

c********************************************************************
             


cF2                           Extended Trigonometric Function
*
*                          Initial Point: [0.2, 0.2, ....,0.2].

2     continue      
      s= float(n)
      do i=1,n
        s = s - dcos(x(i))
      end do   
      do i=1,n
        temp(i) = s + float(i)*(1.d0-dcos(x(i))) - dsin(x(i))
      end do 

      f = 0.d0
      do i=1,n
        f = f + temp(i)**2
      end do
      return
**************************************************************



cF3                    Extended Rosenbrock function
*
* Initial point: [-1.2, 1, -1.2, 1, ..........., -1.2, 1]      
      
3     continue                           
      c=100.d0                           
                           
      f=0.d0
      do i=1,n/2
        f = f + c*(x(2*i)-x(2*i-1)**2)**2 + (1.d0-x(2*i-1))**2
      end do  
     
      return

c******************************************************************



cF4                    Extended White & Holst function
*
*           Initial point: [-1.2, 1, -1.2, 1, ..........., -1.2, 1]      
      
4     continue                           
      c=100.d0                           
                           
      f=0.d0
      do i=1,n/2
        f = f + c*(x(2*i)-x(2*i-1)**3)**2 + (1.d0-x(2*i-1))**2
      end do
      return

c******************************************************************



*
cF5                  Extended Beale Function  U63 (MatrixRom)
*
*                   Initial Point: [1, 0.8, ...., 1, 0.8]
*--------------------------------------------------------------
*
5     continue
*      
      f=0.d0
    
      do i=1,n/2 
        t1=1.5d0  -x(2*i-1)+x(2*i-1)*x(2*i)
        t2=2.25d0 -x(2*i-1)+x(2*i-1)*x(2*i)*x(2*i)
        t3=2.625d0-x(2*i-1)+x(2*i-1)*x(2*i)*x(2*i)*x(2*i)   
        
        f = f + t1*t1 + t2*t2 + t3*t3
    
      end do

      return



cF6                   Extended Penalty Function  U52 (MatrixRom)
*
*                           Intial Point: [1,2,3,.....,n].
*
6     continue      
      temp1=0.d0
      do i=1,n
        temp1 = temp1 + x(i)**2
      end do
*      
      f = (temp1 - 0.25d0)**2
      
      do i=1,n-1
        f = f + (x(i)-1.d0)**2 
      end do

      return


cF7                        Perturbed Quadratic function
*
*                     Initial Point:  [0.5, 0.5, ......, 0.5].
*     
7     continue
      
      temp1 = 0.d0
      do i=1,n
        temp1 = temp1 + x(i)
      end do  
      
      f = temp1*temp1/100.d0
      
      do i=1,n
        f = f + float(i)*x(i)**2      
      end do    
      return



cF8                        Raydan 1 Function
*
*                   Initial point: [1, 1, ..., 1]

8     continue
      
      f=0.d0
      do i=1,n
        f = f + float(i) * (dexp(x(i))-x(i)) / 10.d0
      end do
      return




cF9                             Raydan 2 Function
*
*                       Initial Point: [1, 1, .....,1]
*  
9     continue
      
      f=0.d0
      do i=1,n
        f = f + dexp(x(i)) - x(i)
      end do
      return




cF10                             Diagonal1 Function
*
*                       Initial Point: [1/n, 1/n, ....., 1/n]
*  
10    continue
      
      f=0.d0
      
      do i=1,n
        f = f + dexp(x(i)) - x(i)*float(i)
      end do
      return



cF11                             Diagonal2 Function
*
*                       Initial Point: [1/1, 1/2, ....., 1/n]
*  
11    continue
      
      f=0.d0
      
      do i=1,n
        f = f + dexp(x(i)) - x(i)/float(i)
      end do
      return



cF12                         Diagonal3 Function
*
*                        Initial Point: [1,1,...,1] 

12    continue
      
      f=0.d0
      do i=1,n
        f = f + dexp(x(i)) - float(i)*dsin(x(i))
      end do
      return



cF13                          Hager Function
*
*                        Initial Point: [1,1,...,1] 


13    continue
      f=0.d0
      do i=1,n
        f = f + dexp(x(i)) - x(i)*sqrt(float(i))
      end do
      return



cF14                 Generalized Tridiagonal-1 Function
*
*                        Initial Point: [2,2,...,2] 


14    continue
      
      f=0.d0
      do i=1,n-1
        u(i) = x(i) + x(i+1) - 3.d0
        v(i) = x(i) - x(i+1) + 1.d0
      end do
      do i=1,n-1
        f = f + u(i)**2 + v(i)**4
      end do                      
      return



cF15                   Extended Tridiagonal-1 Function
*
*                        Initial Point: [2,2,...,2] 


15    continue
      
      f=0.d0
      do i=1,n/2
        u(i) = x(2*i-1) + x(2*i) - 3.d0
        v(i) = x(2*i-1) - x(2*i) + 1.d0
      end do
      do i=1,n/2
        f = f + u(i)**2 + v(i)**4
      end do                     
      return



cF16                        Extended Three Exponential Terms
*
*                          Intial Point: [0.1,0.1,.....,0.1].
*

16    continue
      f=0.d0
      do i=1,n/2
        t1=x(j) + 3.d0*x(j+1) - 0.1d0
        t2=x(j) - 3.d0*x(j+1) - 0.1d0
        t3=-x(j) - 0.1d0 
        f = f + dexp(t1) + dexp(t2) + dexp(t3)    
      end do  
      return




cF17                       Generalized Tridiagonal-2  
*                      
*                    Initial point: [-1, -1, .........., -1., -1] 
 
17    continue
      f = 0.d0
      u(1) = 5.d0*x(1)-3.d0*x(1)**2-x(1)**3-3.d0*x(2)+1.d0
      
      do i=2,n-1
        u(i)=5.d0*x(i)-3.d0*x(i)**2-x(i)**3-x(i-1)-3.d0*x(i+1)+1.d0
      end do      
                
      u(n)=5.d0*x(n)-3.d0*x(n)**2-x(n)**3-x(n-1)+1.d0        
*      
      do i=1,n   
        f = f + u(i)**2
        v(i) = 5.d0 -6.d0*x(i) -3.d0*x(i)**2
      end do

      return



cF18                       Diagonal4 Function  
*                      
*                  Initial point: [1, 1, .........., 1., 1] 
 
18    continue
      
      c=100.d0
      f = 0.d0
      do i=1,n/2
        f = f + (x(2*i-1)**2 + c*x(2*i)**2)/2.d0
      end do                  

      return


cF19                       Diagonal5 Function  (MatrixRom)
*                      
*                    Initial point: [1.1, 1.1, .........., 1.1] 
 
19    continue
      
      f = 0.d0
      do i=1,n
        f = f + dlog( dexp(x(i)) + dexp(-x(i)) )
      end do                  
      return




cF20                         Extended Himmelblau Function
*
*                           Initial Point: [1, 1, ....., 1]

20    continue
      
      f=0.d0
      do i=1,n/2
        u1 = x(2*i-1)**2 + x(2*i)    - 11.d0
        v1 = x(2*i-1)    + x(2*i)**2 - 7.d0 
        
        f = f + u1*u1 + v1*v1
      end do
      return




cF21                    Generalized PSC1 Function
*
*                   Initial point: [3, 0.1, ..., 3, 0.1]

21    continue

      f = 0.d0
      do i=1,n-1
        f = f + (x(i)**2 +x(i+1)**2 +x(i)*x(i+1))**2 
     *        + (dsin(x(i)))**2 + (dcos(x(i+1)))**2    
      end do
      return



cF22                       Extended PSC1 Function
*
*                   Initial point: [3, 0.1, ..., 3, 0.1]

22    continue 

      f = 0.d0
      do i=1,n/2
        f = f + (x(2*i-1)**2 +x(2*i)**2 +x(2*i-1)*x(2*i))**2 
     *        + (dsin(x(2*i-1)))**2 + (dcos(x(2*i)))**2    
      end do
      return




cF23                              Extended Powell 
*
*                      Initial Point: [3, -1, 0, 1, ......].
*-------------------------------------------------------------------
*
23    continue
      
      f=0.d0
      
      do i=1,n/4  
        t1= x(4*i-3) + 10.d0*x(4*i-2) 
        t2= x(4*i-1) - x(4*i)
        t3= x(4*i-2) - 2.d0*x(4*i-1)
        t4= x(4*i-3) - x(4*i)     
       
        f = f + t1*t1 + 5.d0*t2*t2 + t3**4 + 10.d0*t4**4          
      end do               
      return



cF24                  Extended Block Diagonal BD1 Function 
*
*                     Initial Point: [0.1, 0.1, ..., 0.1].
*-------------------------------------------------------------------
*
24    continue
      
      f = 0.d0

      do i=1,n/2
        t1 = x(2*i-1)**2 + x(2*i)**2 - 2.d0
        t2 = dexp(x(2*i-1)) - x(2*i)
        
        f = f + t1*t1 + t2*t2
      end do
      return



cF25                           Extended Maratos Function 
*
*                      Initial Point: [1.1, 0.1, ...,1.1, 0.1].
*-------------------------------------------------------------------
*
25    continue
      
      c = 100.d0
      f = 0.d0
      
      do i=1,n/2
        t1 = x(2*i-1)**2 + x(2*i)**2 - 1.d0
        
        f = f + (x(2*i-1) + c*t1*t1)
      end do
      return




cF26                             Extended Cliff                  
*
*                     Initial Point:  [0, -1, ......, 0, -1].
*                     

26    continue
      
      f=0.d0

      do i=1,n/2
          temp1 = (x(2*i-1)-3.d0)/100.d0
      f = f+temp1*temp1-(x(2*i-1)-x(2*i))+dexp(20.d0*(x(2*i-1)-x(2*i)))
      end do
      
      return      




cF27                   Quadratic Diagonal Perturbed Function
*
*                     Initial Point:  [0.5, 0.5, ......, 0.5].
*     
27    continue
      
      temp1 = 0.d0
      do i=1,n
        temp1 = temp1 + x(i)
      end do  
      
      f = temp1*temp1
      
      do i=1,n
        f = f + (float(i)/100.d0) * x(i)**2      
      end do    
      return



cF28                           Extended Wood Function
*
*                         Initial Point: [-3,-1,-3,-1,......]
*
28    continue
      
      f=0.d0
      
      do i=1,n/4
        f = f + 100.d0*(x(4*i-3)**2-x(4*i-2))**2 
     *        +        (x(4*i-3)-1.d0)**2  
     *        +  90.d0*(x(4*i-1)**2-x(4*i))**2            
     *        +        (1.d0-x(4*i-1))**2
     *        +  10.1d0*(x(4*i-2)-1.d0)**2
     *        +  10.1d0*(x(4*i)  -1.d0)**2
     *        +  19.8d0*(x(4*i-2)-1.d0)*(x(4*i)-1.d0)
     
      end do
      return




cF29                          Extended Hiebert Function
*
*                             Initial Point: [0,0,...0].

29    continue
      
      c1=10.d0
      c2=500.d0
      
      f = 0.d0

      do i=1,n/2
        f = f + (x(2*i-1)-c1)**2 + (x(2*i-1)*x(2*i)-c2)**2
      end do
      return



cF30                           Quadratic Function QF1
*
*                             Initial Point: [1,1,...1].

30    continue
      
      f = 0.d0
      
      do i=1,n
        f = f + float(i)*x(i)*x(i)
      end do
      
      f = f/2.d0
      f = f - x(n)
            
      return



cF31                Extended Quadratic Penalty QP1 Function           
*
*                     Initial Point:  [1, 1, ......,1].
*                     

31    continue
      
      t1=0.d0
      do i=1,n
        t1 = t1 + x(i)*x(i)
      end do
      t1 = t1 - 0.5d0  
                   
      f = 0.d0                   
      do i=1,n-1
        f = f + (x(i)*x(i) - 2.d0)**2
      end do  
      
      f = f + t1*t1
      
      return



cF32               Extended Quadratic Penalty QP2 Function           
*
*                     Initial Point:  [1, 1, ......,1].
*                     

32    continue
      
      t1=0.d0
      do i=1,n
        t1 = t1 + x(i)*x(i)
      end do
      t1 = t1 - 100.d0  
                   
      f = 0.d0                   
      do i=1,n-1
        f = f + (x(i)*x(i) - dsin(x(i)))**2
      end do  
      
      f = f + t1*t1
      
      return




cF33                      A Quadratic Function QF2
*
*                     Initial Point:  [0.5, 0.5, ......,0.5].
*                     

33    continue
      
      f=0.d0
      do i=1,n 
        f = f + float(i)*(x(i)**2 - 1.d0)**2
      end do
      f = f/2.d0
      f = f - x(n)

      
      return



cF34                       Extended EP1 Function 
*
*                      Initial Point: [1.5.,1.5.,...,1.5].
*-------------------------------------------------------------------
*
34    continue
*      
      f=0.d0
                               
      do i=1,n/2                                     
        t1=dexp(x(2*i-1)-x(2*i)) - 5.d0        
        t2=x(2*i-1)-x(2*i)
        t3=x(2*i-1)-x(2*i)-11.d0
        
        f = f + t1*t1 + (t2*t2)*(t3*t3)
      end do  
      
      return




cF35                  Extended Tridiagonal-2 Function 
*
*                      Initial Point: [1.,1.,...,1.].
*-------------------------------------------------------------------
*
35    continue
*      
      c=0.1d0
      f=0.d0
      
      do i=1,n-1                                    
        f = f + (x(i)*x(i+1)-1.d0)**2 + c*(x(i)+1.d0)*(x(i+1)+1.d0)
      end do

      return





cF36                           BDQRTIC (CUTE)
*
*                     Initial point x0=[1.,1.,...,1.].
*
*
36    continue
*        
      n4=n-4
      f=0.d0
        
      do i=1,n4
        temp(i) = x(i)**2 + 2.d0*x(i+1)**2 + 3.d0*x(i+2)**2 
     *                    + 4.d0*x(i+3)**2 + 5.d0*x(n)**2
      end do           
        
      do i=1,n4
        f = f + (-4.d0*x(i)+3.d0)**2 + temp(i)**2
      end do                                  
            
      return




cF37                           TRIDIA  (CUTE)                             
*
*                     Initial point x0=[1,1,...,1].
*
*
37    continue
*                
      alpha=2.d0
      beta=1.d0
      gamma=1.d0
      delta=1.d0
        
      f=gamma*(delta*x(1)-1.d0)**2
      
      do i=2,n
        f = f + float(i)*(alpha*x(i)-beta*x(i-1))**2
      end do
      return




cF38                       ARWHEAD  (CUTE)                             
*                          
*                     Initial point x0=[1,1,...,1].
*
*
38    continue

      f=0.d0
      do i=1,n-1
        f = f + (-4.d0*x(i)+3.d0) + (x(i)**2+x(n)**2)**2
      end do
                 
      
      return




cF39                                                         
*                       NONDIA (Shanno-78)  (CUTE)
*
*                     Initial point x0=[-1,-1,...,-1].
*
*
39    continue

      c=100.d0
      
      f=(x(1)-1.d0)**2 + c*(x(1)-x(1)**2)**2
      
      do i=2,n
        f = f + c*(x(1)-x(i)**2)**2
      end do
              
      return



cF40                          NONDQUAR  (CUTE)                             
*                             
*                     Initial point x0=[1,-1,1,-1,...,].
*
*
40    continue

      f = (x(1)-x(2))**2 + (x(n-1)+x(n))**2      
      
      do i=1,n-2
        f = f + (x(i)+x(i+1)+x(n))**4
      end do
              
      return



cF41                          DQDRTIC                              
*                             
*                     Initial point x0=[3,3,3...,3].
*
*
41    continue
      
      c=100.d0
      d=100.d0
      
      f=0.d0
      do i=1,n-2
        f = f + (x(i)**2 + c*x(i+1)**2 + d*x(i+2)**2)
      end do
      
      return
       



cF42                          EG2 (CUTE)                             
*                             
*                     Initial point x0=[1,1,1...,1].
*
*
42    continue

      f=0.5d0*dsin(x(n)*x(n))
      do i=1,n-1
        f = f + dsin(x(1)+x(i)*x(i)-1.d0)
      end do

      return


cF43                        DIXMAANA (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
43    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
     
        return
                                        



cF44                         DIXMAANB (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
44    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0625d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
     
      return
                                        



cF45                        DIXMAANC  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
45    continue
*    
        alpha = 1.d0
        beta  = 0.125d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do

     
      return




cF46                         DIXMAANE  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
46    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do

        return



cF47                       Partial Perturbed Quadratic                              
*                             
*                    Initial point x0=[0.5, 0.5, ..., 0.5].
*
47    continue
*    
        temp(1) = x(1) + x(2)
        do i=2,n-1
          temp(i) = temp(i-1) + x(i+1)
        end do              
        
        f=x(1)*x(1)
        do i=2,n
          f = f + float(i)*x(i)*x(i) + temp(i-1)*temp(i-1)/100.d0
        end do

      return


cF48                       Broyden Tridiagonal                              
*                             
*                  Initial point x0=[-1., -1., ..., -1.].
*
48    continue
*

      temp(1) = 3.d0*x(1) - 2.d0*x(1)*x(1)
      do i=2,n-1
        temp(i) = 3.d0*x(i)-2.d0*x(i)*x(i)-x(i-1)-2.d0*x(i+1)+1.d0
      end do
        
      temp(n) = 3.d0*x(n)-2.d0*x(n)*x(n)-x(n-1)+1.d0
      
      f = 0.d0
      
      do i=1,n
        f = f + temp(i)*temp(i)
      end do
      
      return
    


cF49                   Almost Perturbed Quadratic                              
*                             
*                  Initial point x0=[0.5, 0.5, ...,0.5].
*
49    continue
*    

      f = 0.01d0*(x(1)+x(n))**2
      
      do i=1,n
        f = f + float(i)*x(i)*x(i)
      end do
      
      return
      


cF50                   Tridiagonal Perturbed Quadratic                              
*                             
*                    Initial point x0=[0.5, 0.5, ...,0.5].
*
50    continue
*

      do i=1,n-2
        temp(i) = x(i) + x(i+1) + x(i+2)
      end do
      
      f = x(1)*x(1)
      do i=2,n-1
        f = f + float(i)*x(i)*x(i) + temp(i-1)**2
      end do
      
      return  


cF51                       EDENSCH Function  (CUTE)
*
*                      Initial Point: [0., 0., ..., 0.].
*-------------------------------------------------------------------
*

51    continue      

      f = 16.d0

      do i=1,n-1
        f = f + (x(i)-2.d0)**4 + 
     *          (x(i)*x(i+1)-2.d0*x(i+1))**2 +
     *          (x(i+1)+1.d0)**2
      end do
          
     
      return
    
    
cF52                        VARDIM Function  (CUTE)
*
*                      Initial Point: [1-1/n, 1-2/n, ..., 1-n/n].
*-------------------------------------------------------------------
*

52    continue

      sum = 0.d0
      do i=1,n
        sum = sum + float(i)*x(i)
      end do
      
      sum = sum - float(n)*float(n+1)/2.d0
      
      f = sum**2 + sum**4
      do i=1,n               
        f = f + (x(i)-1.d0)**2
      end do  
      
      return
      


cF53                           STAIRCASE S1                               
*
*                     Initial point x0=[1,1,...,1].
*

53    continue

	  f=0.d0
      do i=1,n-1
        f = f + (x(i)+x(i+1)-float(i))**2
      end do             	  
      
      return





cF54                             LIARWHD (CUTE)                              
*                             
*                      Initial point x0=[4., 4., ....4.].
*

54    continue                  

      f=0.d0
      do i=1,n
        f = f + 4.d0*(x(i)*x(i) - x(1))**2 + (x(i)-1.d0)**2
      end do  
        
      return



cF55                        DIAGONAL 6                            
*                                
*                Initial point x0=[1.,1., ..., 1.].
*

55    continue   
        
      f = 0.d0
      
      do i=1,n
        f = f + dexp(x(i)) - (1.d0+x(i))
      end do                                        
      return
       
       
cF56                       DIXON3DQ  (CUTE)
*
*                Initial Point x0=[-1, -1,..., -1]  March 7, 2005
*
56    continue

      f=(x(1)-2.d0)**2
      
      do i=1,n-1
        f = f + (x(i)-x(i+1))**2
      end do
      
      f = f + (x(n)-1.d0)**2
      return
      



cF57                        DIXMAANF (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
57    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0625d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
     
        return


cF58                         DIXMAANG  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
58    continue
*    
        alpha = 1.d0
        beta  = 0.125d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 1
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do

     
        return



cF59                         DIXMAANH  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
59    continue
*    
        alpha = 1.d0
        beta  = 0.26d0
        gamma = 0.26d0
        delta = 0.26d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
     
        return




cF60                        DIXMAANI  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
60    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 2
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do

        return




cF61                        DIXMAANJ  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
61    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do

        return




cF62                         DIXMAANK  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
62    continue
*    
        alpha = 1.d0
        beta  = 0.625d0
        gamma = 0.625d0
        delta = 0.625d0
                      
        k1 = 1
        k2 = 1
        k3 = 1
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
     
        return





cF63                          DIXMAANL  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
63    continue
*    
        alpha = 1.d0
        beta  = 0.26d0
        gamma = 0.26d0
        delta = 0.26d0
                      
        k1 = 2
        k2 = 2
        k3 = 2
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do

        return




cF64                         ENGVAL1 (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
64    continue                          
      
      do i=1,n-1
        t(i) = x(i)*x(i) + x(i+1)*x(i+1)
      end do
      
      f = 0.d0
      
      do i=1,n-1
        f = f + t(i)*t(i) + (-4.d0*x(i) + 3.d0)
      end do   
      
      return      



cF65                         FLETCHCR (CUTE)
*
*                       Initial point: [0,0,...,0]
*
65    continue

      f=0.d0    
      
      do i=1,n-1
        t(i) = x(i+1) - x(i) + 1.d0 - x(i)*x(i)
      end do
      
      do i=1,n-1
        f = f + 100.d0*t(i)*t(i)
      end do

      return            




cF66                          COSINE (CUTE)
* 
*                      Initial point: [1,1,...,1]

66    continue

      f=0.d0
      do i=1,n-1
        f = f + dcos(x(i)*x(i) - x(i+1)/2.d0)
      end do
      
      return          



cF67                   DENSCHNB  (CUTE)
*
*               Initial point: [1,1,...,1]

67    continue

      f=0.d0
      do i=1,n/2
        f = f + (x(2*i-1)-2.d0)**2 +
     *          ((x(2*i-1)-2.d0)**2)*(x(2*i)**2) +
     *          (x(2*i)+1.d0)**2
      end do
      
      return  


cF68                  DENSCHNF  (CUTE)
*
*               Initial point: [2,0,2,0,...,2,0]

68    continue

      f=0.d0
      do  i=1,n/2
        f=f+(2.d0*(x(2*i-1)+x(2*i))**2+(x(2*i-1)-x(2*i))**2-8.d0)**2+
     *      (5.d0*x(2*i-1)**2+(x(2*i)-3.d0)**2-9.d0)**2
      end do       
      return
      
      
cF69                     SINQUAD (CUTE)
*                    
*             Initial Point: [0.1, 0.1, ..., 0.1]

69    continue

      f=(x(1)-1.d0)**4 + (x(n)**2-x(1)**2)**2
      
      do i=1,n-2
        t(i) = sin(x(i+1)-x(n)) - x(1)**2 + x(i+1)**2
        f = f + t(i)*t(i)
      end do

      return


cF70                     BIGGSB1 (CUTE)
*                    
*               Initial Point: [0., 0., ....,0.]

70    continue
      
      f=(x(1)-1.d0)**2 + (1.d0-x(n))**2
      do i=2,n
        f = f + (x(i)-x(i-1))**2
      end do
      return 

cF71                     Partial Perturbed Quadratic PPQ2
*
*                        Initial Point: [0.5,0.5,...,0.5] 

71	  continue

        temp(1) = x(1) + x(2)
        do i=2,n-1
          temp(i) = temp(i-1) + x(i+1)
        end do              
        
        f=x(1)*x(1)
        do i=2,n
          f = f + x(i)*x(i)/float(i) + temp(i-1)*temp(i-1)/100.d0
        end do

      end         
c-------------------------------------------------------- End EVALFG
      
      
      
      subroutine evalg(n,x,g, nexp)
      real*8 x(n), g(n)  
      
      real*8 t1,t2,t3,t4, c, d
      real*8 s, temp(11000), temp1, tsum, sum
      real*8 u(11000), v(11000), t(11000) 
      real*8 u1, v1, c1, c2
      real*8 alpha, beta, gamma, delta
                       
      integer k1, k2, k3, k4      
                             
*      
      go to ( 1, 2, 3, 4, 5, 6, 7, 8, 9,10,
     *       11,12,13,14,15,16,17,18,19,20,
     *       21,22,23,24,25,26,27,28,29,30,
     *       31,32,33,34,35,36,37,38,39,40,
     *       41,42,43,44,45,46,47,48,49,50,
     *       51,52,53,54,55,56,57,58,59,60,
     *       61,62,63,64,65,66,67,68,69,70,
     *       71) nexp  
     
     

cF1                       Extended Freudenstein & Roth
*
*                      Initial Point: [0.5, -2, ...,0.5, -2].
*

1     continue
      j=1         
      
      do i=1,n/2 
        t1=-13.d0+x(2*i-1)+5.d0*x(2*i)*x(2*i)-x(2*i)**3-2.d0*x(2*i) 
        t2=-29.d0+x(2*i-1)+x(2*i)**3+x(2*i)**2-14.d0*x(2*i)
        
        g(j)  =2.d0*(t1+t2)   
        g(j+1)=2.d0*t1*(10.d0*x(2*i)-3.d0*x(2*i)*x(2*i)-2.d0) +
     *            2.d0*t2*(3.d0*x(2*i)*x(2*i)+2.d0*x(2*i)-14.d0)   
        j=j+2        
      end do   
      
      return

c********************************************************************
             


cF2                           Extended Trigonometric Function
*
*                          Initial Point: [0.2, 0.2, ....,0.2].

2     continue      
      s= float(n)
      do i=1,n
        s = s - dcos(x(i))
      end do   
      do i=1,n
        temp(i) = s + float(i)*(1.d0-dcos(x(i))) - dsin(x(i))
      end do
      
      s=0.d0
      do i=1,n
        s = s + temp(i)
      end do
      
      do i=1,n
        g(i) = 2.d0*s*dsin(x(i)) + 
     +            2.d0*temp(i)*(float(i)*dsin(x(i))-dcos(x(i)))  
      end do      
      return
**************************************************************



cF3                    Extended Rosenbrock function
*
* Initial point: [-1.2, 1, -1.2, 1, ..........., -1.2, 1]      
      
3     continue                           
      c=100.d0                           
   
      j=1
      do i=1,n/2
        g(j)   = -4.d0*c*x(2*i-1)*(x(2*i)-x(2*i-1)**2) - 
     *            2.d0*(1.d0-x(2*i-1))
        g(j+1) =  2.d0*c*(x(2*i)-x(2*i-1)**2)
        j = j + 2
      end do
      
      return

c******************************************************************



cF4                    Extended White & Holst function
*
*           Initial point: [-1.2, 1, -1.2, 1, ..........., -1.2, 1]      
      
4     continue                           
      c=100.d0                           

      j=1
      do i=1,n/2
        g(j)   = -6.d0*c*x(2*i-1)*x(2*i-1)*(x(2*i)-x(2*i-1)**3) - 
     *            2.d0*(1.d0-x(2*i-1))
        g(j+1) =  2.d0*c*(x(2*i)-x(2*i-1)**3)
        j = j + 2
      end do
      
      return

c******************************************************************



*
cF5                  Extended Beale Function  U63 (MatrixRom)
*
*                   Initial Point: [1, 0.8, ...., 1, 0.8]
*--------------------------------------------------------------
*
5     continue
*      
      j=1      
      do i=1,n/2 
        t1=1.5d0  -x(2*i-1)+x(2*i-1)*x(2*i)
        t2=2.25d0 -x(2*i-1)+x(2*i-1)*x(2*i)*x(2*i)
        t3=2.625d0-x(2*i-1)+x(2*i-1)*x(2*i)*x(2*i)*x(2*i)   

        g(j)  =2.d0*t1*(-1.d0+x(2*i)) +
     *         2.d0*t2*(-1.d0+x(2*i)**2) +
     *         2.d0*t3*(-1.d0+x(2*i)**3)   
        g(j+1)=2.d0*t1*x(2*i-1) +
     *         2.d0*t2*2.d0*x(2*i-1)*x(2*i) +
     *         2.d0*t3*3.d0*x(2*i-1)*x(2*i)*x(2*i)   
        j=j+2        
      end do   
*   
      return



cF6                   Extended Penalty Function  U52 (MatrixRom)
*
*                           Intial Point: [1,2,3,.....,n].
*
6     continue      
      temp1=0.d0
      do i=1,n
        temp1 = temp1 + x(i)**2
      end do

      do i=1,n-1
        g(i) = 2.d0*(x(i)-1.d0) + 4.d0*x(i)*(temp1-0.25d0)
      end do                                              
      
      g(n) = 4.d0*x(n)*(temp1-0.25d0)
      return


cF7                        Perturbed Quadratic function
*
*                     Initial Point:  [0.5, 0.5, ......, 0.5].
*     
7     continue
      
      temp1 = 0.d0
      do i=1,n
        temp1 = temp1 + x(i)
      end do  
      
      do i=1,n
        g(i) = float(i) * 2.d0 * x(i) + temp1/50.d0
      end do        
      return



cF8                        Raydan 1 Function
*
*                   Initial point: [1, 1, ..., 1]

8     continue
     
      do i=1,n
        g(i) = float(i) * (dexp(x(i)) - 1.d0) / 10.d0      
      end do  
      return




cF9                             Raydan 2 Function
*
*                       Initial Point: [1, 1, .....,1]
*  
9     continue

      do i=1,n
        g(i) = dexp(x(i)) - 1.d0
      end do
      return




cF10                             Diagonal1 Function
*
*                       Initial Point: [1/n, 1/n, ....., 1/n]
*  
10    continue
   
      do i=1,n
        g(i) = dexp(x(i)) - float(i)
      end do
      return



cF11                             Diagonal2 Function
*
*                       Initial Point: [1/1, 1/2, ....., 1/n]
*  
11    continue

      do i=1,n
        g(i) = dexp(x(i)) - 1.d0/float(i)
      end do
      return



cF12                         Diagonal3 Function
*
*                        Initial Point: [1,1,...,1] 

12    continue

      do i=1,n
        g(i) = dexp(x(i)) - float(i)*dcos(x(i))
      end do
      return



cF13                          Hager Function
*
*                        Initial Point: [1,1,...,1] 


13    continue

      do i=1,n
        g(i) = dexp(x(i)) - sqrt(float(i))
      end do
      return



cF14                 Generalized Tridiagonal-1 Function
*
*                        Initial Point: [2,2,...,2] 


14    continue

      do i=1,n-1
        u(i) = x(i) + x(i+1) - 3.d0
        v(i) = x(i) - x(i+1) + 1.d0
      end do

      g(1) = 2.d0*u(1) + 4.d0*v(1)**3
      do i=2,n-1
        g(i) = 2.d0*u(i-1) - 4.d0*v(i-1)**3 + 2.d0*u(i) + 4.d0*v(i)**3
      end do
      g(n) = 2.d0*u(n-1) - 4.d0*v(n-1)**3                   
      return



cF15                   Extended Tridiagonal-1 Function
*
*                        Initial Point: [2,2,...,2] 


15    continue
      
      do i=1,n/2
        u(i) = x(2*i-1) + x(2*i) - 3.d0
        v(i) = x(2*i-1) - x(2*i) + 1.d0
      end do
                  
      j=1
      do i=1,n/2
        g(j)   = 2.d0*u(i) + 4.d0*v(i)**3
        g(j+1) = 2.d0*u(i) - 4.d0*v(i)**3
        j=j+2
      end do                        
      return



cF16                        Extended Three Exponential Terms
*
*                          Intial Point: [0.1,0.1,.....,0.1].
*

16    continue
      j=1
      do i=1,n/2
        t1=x(j) + 3.d0*x(j+1) - 0.1d0
        t2=x(j) - 3.d0*x(j+1) - 0.1d0
        t3=-x(j) - 0.1d0
        g(j)   = dexp(t1) + dexp(t2) - dexp(t3)
        g(j+1) = 3.d0*dexp(t1) - 3.d0*dexp(t2)        
        j=j+2
      end do  
      return




cF17                       Generalized Tridiagonal-2  
*                      
*                    Initial point: [-1, -1, .........., -1., -1] 
 
17    continue
      u(1) = 5.d0*x(1)-3.d0*x(1)**2-x(1)**3-3.d0*x(2)+1.d0
      
      do i=2,n-1
        u(i)=5.d0*x(i)-3.d0*x(i)**2-x(i)**3-x(i-1)-3.d0*x(i+1)+1.d0
      end do      
                
      u(n)=5.d0*x(n)-3.d0*x(n)**2-x(n)**3-x(n-1)+1.d0        
*      
      do i=1,n   
        v(i) = 5.d0 -6.d0*x(i) -3.d0*x(i)**2
      end do

      g(1) = 2.d0*u(1)*v(1) - 2.d0*u(2)
      
      do i=2,n-1
        g(i) = -6.d0*u(i-1) + 2.d0*u(i)*v(i) -2.d0*u(i+1)
      end do
      
      g(n) = -6.d0*u(n-1) + 2.d0*u(n)*v(n)  
      return



cF18                       Diagonal4 Function  
*                      
*                  Initial point: [1, 1, .........., 1., 1] 
 
18    continue
      
      c=100.d0

      j=1
      do i=1,n/2
        g(j)   = x(2*i-1)
        g(j+1) = c*x(2*i)
        j=j+2
      end do  
      return


cF19                       Diagonal5 Function  (MatrixRom)
*                      
*                    Initial point: [1.1, 1.1, .........., 1.1] 
 
19    continue
      
      do i=1,n
        g(i) = (dexp(x(i)) - dexp(-x(i)))/(dexp(x(i)) + dexp(-x(i)))
      end do  
      return




cF20                         Extended Himmelblau Function
*
*                           Initial Point: [1, 1, ....., 1]

20    continue

      j=1
      do i=1,n/2
        u1 = x(2*i-1)**2 + x(2*i)    - 11.d0
        v1 = x(2*i-1)    + x(2*i)**2 - 7.d0 

        g(j)   = 4.d0*u1*x(2*i-1) + 2.d0*v1
        g(j+1) = 2.d0*u1          + 4.d0*v1*x(2*i)
        j=j+2
      end do
      return




cF21                    Generalized PSC1 Function
*
*                   Initial point: [3, 0.1, ..., 3, 0.1]

21    continue

      g(1)=   2.d0*(x(1)**2+x(2)**2+x(1)*x(2))*(2.d0*x(1)+x(2)) 
     *       +2.d0 * dsin(x(1)) * dcos(x(1))
      
      do i=2,n-1
      g(i) =  2.d0*(x(i-1)**2+x(i)**2+x(i-1)*x(i))*(2.d0*x(i)+x(i-1))
     +       +2.d0*(x(i)**2+x(i+1)**2+x(i)*x(i+1))*(2.d0*x(i)+x(i+1))
      end do

      g(n) =  2.d0*(x(n-1)**2+x(n)**2+x(n-1)*x(n))*(2.d0*x(n)+x(n-1))
     +       -2.d0*dcos(x(n)) * dsin(x(n)) 
      return



cF22                       Extended PSC1 Function
*
*                   Initial point: [3, 0.1, ..., 3, 0.1]

22    continue 

      j=1
      do i=1,n/2
      g(j)   = 2.d0*(x(2*i-1)**2+x(2*i)**2+x(2*i-1)*x(2*i)) *
     +              (2.d0*x(2*i-1)+x(2*i)) +
     +              2.d0*(dsin(x(2*i-1)))*(dcos(x(2*i-1)))
     
      g(j+1) = 2.d0*(x(2*i-1)**2+x(2*i)**2+x(2*i-1)*x(2*i)) *
     +              (2.d0*x(2*i)+x(2*i-1)) -
     +              2.d0*(dcos(x(2*i)))*(dsin(x(2*i)))
      j=j+2
      end do
      return




cF23                              Extended Powell 
*
*                      Initial Point: [3, -1, 0, 1, ......].
*-------------------------------------------------------------------
*
23    continue
      
      j=1    
      do i=1,n/4  
        t1= x(4*i-3) + 10.d0*x(4*i-2) 
        t2= x(4*i-1) - x(4*i)
        t3= x(4*i-2) - 2.d0*x(4*i-1)
        t4= x(4*i-3) - x(4*i)     

        g(j)  =   2.d0*t1 + 40.d0*t4**3
        g(j+1)=  20.d0*t1 +  4.d0*t3**3
        g(j+2)=  10.d0*t2 -  8.d0*t3**3
        g(j+3)= -10.d0*t2 - 40.d0*t4**3
        
        j=j+4        
      end do               
      return



cF24                  Extended Block Diagonal BD1 Function 
*
*                     Initial Point: [0.1, 0.1, ..., 0.1].
*-------------------------------------------------------------------
*
24    continue

      j=1
      do i=1,n/2
        t1 = x(2*i-1)**2 + x(2*i)**2 - 2.d0
        t2 = dexp(x(2*i-1)) - x(2*i)
        
        g(j)   = 4.d0*t1*x(2*i-1) + 2.d0*t2*dexp(x(2*i-1)-1.d0)
        g(j+1) = 4.d0*t1*x(2*i) - 2.d0*t2
        j=j+2
      end do
      return



cF25                           Extended Maratos Function 
*
*                      Initial Point: [1.1, 0.1, ...,1.1, 0.1].
*-------------------------------------------------------------------
*
25    continue
      
      c = 100.d0
      
      j=1
      do i=1,n/2
        t1 = x(2*i-1)**2 + x(2*i)**2 - 1.d0
        
        g(j)   = 1.d0 + 4.d0 * c * t1 * x(2*i-1)
        g(j+1) =        4.d0 * c * t1 * x(2*i)
        j=j+2
      end do
      return




cF26                             Extended Cliff                  
*
*                     Initial Point:  [0, -1, ......, 0, -1].
*                     

26    continue
        
      j=1  
      do i=1,n/2
        temp1 = (x(2*i-1)-3.d0)/100.d0
                
       g(j)   = temp1/50.d0 - 1.d0 + 20.d0*dexp(20.d0*(x(2*i-1)-x(2*i)))
       g(j+1) = 1.d0 - 20.d0*dexp(20.d0*(x(2*i-1)-x(2*i)))
       j=j+2
      end do
      
      return      




cF27                   Quadratic Diagonal Perturbed Function
*
*                     Initial Point:  [0.5, 0.5, ......, 0.5].
*     
27    continue
      
      temp1 = 0.d0
      do i=1,n
        temp1 = temp1 + x(i)
      end do  
      
      do i=1,n
        g(i) = float(i) * x(i) / 50.d0 + 2.d0*temp1
      end do        
      return



cF28                           Extended Wood Function
*
*                         Initial Point: [-3,-1,-3,-1,......]
*
28    continue

      j=1
      do i=1,n/4
        g(j)   = 400.d0*(x(4*i-3)**2-x(4*i-2))*x(4*i-3)
     *             + 2.d0*(x(4*i-3)-1.d0)
        g(j+1) =-200.d0*(x(4*i-3)**2-x(4*i-2))
     *             + 20.2d0*(x(4*i-2)-1.d0)   
     *             + 19.8d0*(x(4*i)-1.d0)
        g(j+2) = 360.d0*(x(4*i-1)**2-x(4*i))*x(4*i-1)
     *             - 2.d0*(1.d0-x(4*i-1))   
        g(j+3) =-180.d0*(x(4*i-1)**2-x(4*i))
     *             + 20.2d0*(x(4*i)  -1.d0)
     *             + 19.8d0*(x(4*i-2)-1.d0)   
        j=j+4
      end do
      return




cF29                          Extended Hiebert Function
*
*                             Initial Point: [0,0,...0].

29    continue
      
      c1=10.d0
      c2=500.d0

      
      j=1
      do i=1,n/2
          g(j)   =   2.d0*(x(2*i-1)-c1) 
     *           + 2.d0*(x(2*i-1)*x(2*i)-c2)*x(2*i)       
          g(j+1) =   2.d0*(x(2*i-1)*x(2*i)-c2)*x(2*i-1)
        j=j+2
      end do
      return



cF30                           Quadratic Function QF1
*
*                             Initial Point: [1,1,...1].

30    continue
      do i=1,n
          g(i)   =   float(i) * x(i)
      end do
      g(n) = g(n) - 1.d0
            
      return



cF31                Extended Quadratic Penalty QP1 Function           
*
*                     Initial Point:  [1, 1, ......,1].
*                     

31    continue
      
      t1=0.d0
      do i=1,n
        t1 = t1 + x(i)*x(i)
      end do
      t1 = t1 - 0.5d0  
            
      do i=1,n-1
        g(i) = 4.d0*(x(i)*x(i)-2.d0)*x(i) + 4.d0*t1*x(i)
      end do  
      
      g(n) = 4.d0*t1*x(n)
      
      return



cF32               Extended Quadratic Penalty QP2 Function           
*
*                     Initial Point:  [1, 1, ......,1].
*                     

32    continue
      
      t1=0.d0
      do i=1,n
        t1 = t1 + x(i)*x(i)
      end do
      t1 = t1 - 100.d0  
                
      do i=1,n-1
        g(i) = 2.d0*(x(i)*x(i)-dsin(x(i)))*(2.d0*x(i)-dcos(x(i))) 
     *       + 4.d0*t1*x(i)
      end do
      g(n) = 4.d0*t1*x(n)
      
      return




cF33                      A Quadratic Function QF2
*
*                     Initial Point:  [0.5, 0.5, ......,0.5].
*                     

33    continue

      do i=1,n
        g(i) = float(i)*2.d0*x(i)*(x(i)**2 - 1.d0)
      end do
      g(n) = g(n) - 1.d0
      
      return



cF34                       Extended EP1 Function 
*
*                      Initial Point: [1.5.,1.5.,...,1.5].
*-------------------------------------------------------------------
*
34    continue

      j=1                                     
      do i=1,n/2                                     
        t1=dexp(x(2*i-1)-x(2*i)) - 5.d0        
        t2=x(2*i-1)-x(2*i)
        t3=x(2*i-1)-x(2*i)-11.d0
      
        g(j) =   2.d0*t1*dexp(x(2*i-1)-x(2*i)) 
     *         + 2.d0*t2*t3*t3 
     *         + 2.d0*t2*t2*t3         
        g(j+1) = - g(j)
        j=j+2
      end do  
      
      return




cF35                  Extended Tridiagonal-2 Function 
*
*                      Initial Point: [1.,1.,...,1.].
*-------------------------------------------------------------------
*
35    continue
*      
      c=0.1d0
      
      g(1) = 2.d0*(x(1)*x(2)-1.d0)*x(2) + c*(x(2)+1.d0)
      
      do i=2,n-1
        g(i) = 2.d0*(x(i-1)*x(i)-1.d0)*x(i-1) + c*(x(i-1)+1.d0) +
     *         2.d0*(x(i)*x(i+1)-1.d0)*x(i+1) + c*(x(i+1)+1.d0)
      end do 
                                               
      g(n) = 2.d0*(x(n-1)*x(n)-1.d0)*x(n-1) + c*(x(n-1)+1.d0)

      return





cF36                           BDQRTIC (CUTE)
*
*                     Initial point x0=[1.,1.,...,1.].
*
*
36    continue
*        
      n4=n-4
     
      do i=1,n4
        temp(i) = x(i)**2 + 2.d0*x(i+1)**2 + 3.d0*x(i+2)**2 
     *                    + 4.d0*x(i+3)**2 + 5.d0*x(n)**2
      end do           
        
      g(1) = -8.d0*(-4.d0*x(1)+3.d0) + 
     *        (4.d0*temp(1))*x(1)        
      g(2) = -8.d0*(-4.d0*x(2)+3.d0) + 
     *        (8.d0*temp(1)+  4.d0*temp(2))*x(2)
      g(3) = -8.d0*(-4.d0*x(3)+3.d0) + 
     *        (12.d0*temp(1)+ 8.d0*temp(2)+ 4.d0*temp(3))*x(3)
      g(4) = -8.d0*(-4.d0*x(4)+3.d0) + 
     *        (16.d0*temp(1)+12.d0*temp(2) +8.d0*temp(3)+
     *          4.d0*temp(4))*x(4)
        
      do i=5,n4
        g(i) = -8.d0*(-4.d0*x(i)+3.d0) + 
     *         (16.d0*temp(i-3)+12.d0*temp(i-2)+
     *           8.d0*temp(i-1)+4.d0*temp(i))*x(i)
      end do
        
      g(n4+1) =(16.d0*temp(n4-2)+12.d0*temp(n4-1)+8.d0*temp(n4))*x(n4+1)
      g(n4+2) =(16.d0*temp(n4-1)+12.d0*temp(n4))*x(n4+2)
      g(n4+3) =(16.d0*temp(n4))*x(n4+3)
          
      tsum=0.d0
      do i=1,n4
        tsum = tsum + temp(i)
      end do
      g(n) = 20.d0*tsum*x(n)
             
      return




cF37                           TRIDIA  (CUTE)                             
*
*                     Initial point x0=[1,1,...,1].
*
*
37    continue
*                
      alpha=2.d0
      beta=1.d0
      gamma=1.d0
      delta=1.d0
      g(1) = 2.d0*gamma*(delta*x(1)-1.d0)*delta - 
     *       4.d0*(alpha*x(2)-beta*x(1))*beta
     
      do i=2,n-1
        g(i) = 2.d0*float(i)*(alpha*x(i)-beta*x(i-1))*alpha -
     *         2.d0*float(i+1)*(alpha*x(i+1)-beta*x(i))*beta
      end do         

      g(n) = 2.d0*float(n)*(alpha*x(n)-beta*x(n-1))*alpha

      return




cF38                       ARWHEAD  (CUTE)                             
*                          
*                     Initial point x0=[1,1,...,1].
*
*
38    continue
      do i=1,n-1
        g(i) = -4.d0 + 4.d0*x(i)*(x(i)**2+x(n)**2)
      end do
      
      g(n) = 0.d0
      do i=1,n-1
        g(n) = g(n) + 4.d0*x(n)*(x(i)**2+x(n)**2)
      end do                    
      
      return




cF39                                                         
*                       NONDIA (Shanno-78)  (CUTE)
*
*                     Initial point x0=[-1,-1,...,-1].
*
*
39    continue

      c=100.d0

      g(1)=2.d0*(x(1)-1.d0) + 2.d0*c*(x(1)-x(1)**2)*(1.d0-2.d0*x(1))        
      do i=2,n
        g(1) = g(1) + 2.d0*c*(x(1)-x(i)**2)
      end do  
      
      do i=2,n
        g(i) = -4.d0*c*x(i)*(x(1)-x(i)**2)
      end do        
              
      return



cF40                          NONDQUAR  (CUTE)                             
*                             
*                     Initial point x0=[1,-1,1,-1,...,].
*
*
40    continue

      g(1) = 2.d0*(x(1)-x(2))+4.d0*(x(1)+x(2)+x(n))**3
      g(2) =-2.d0*(x(1)-x(2))+4.d0*(x(1)+x(2)+x(n))**3 +
     *                        4.d0*(x(2)+x(3)+x(n))**3
      
      do i=3,n-2
        g(i) = 4.d0*(x(i-1)+x(i)+x(n))**3 +
     *         4.d0*(x(i)+x(i+1)+x(n))**3
      end do          
      
      g(n-1) = 4.d0*(x(n-2)+x(n-1)+x(n))**3 +
     *         2.d0*(x(n-1)+x(n))
      
      g(n) = 2.d0*(x(n-1)+x(n))
      do i=1,n-2
        g(n) = g(n) + 4.d0*(x(i)+x(i+1)+x(n))**3
      end do  
              
      return



cF41                          DQDRTIC                              
*                             
*                     Initial point x0=[3,3,3...,3].
*
*
41    continue
      
      c=100.d0
      d=100.d0

      g(1) = 2.d0*x(1)
      g(2) = 2.d0*c*x(2) + 2.d0*x(2)
      
      do i=3,n-2
        g(i) = 2.d0*(1.d0+d+c)*x(i)
      end do
      
      g(n-1) = 2.d0*(c+d)*x(n-1)          
      g(n) = 2.d0*d*x(n)
      
      return
       



cF42                          EG2 (CUTE)                             
*                             
*                     Initial point x0=[1,1,1...,1].
*
*
42    continue


      g(1)=(1.d0+2.d0*x(1))*dcos(x(1)+x(1)*x(1)-1.d0)
      do i=2,n-1
        g(1) = g(1) + dcos(x(1)+x(i)*x(i)-1.d0)
      end do               
      
      do i=2,n-1
        g(i) = 2.d0*x(i)*dcos(x(1)+x(i)*x(i)-1.d0)
      end do
      
      g(n) = x(n)*dcos(x(n)*x(n))

      return


cF43                        DIXMAANA (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
43    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return
                                        



cF44                         DIXMAANB (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
44    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0625d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
      return
                                        



cF45                        DIXMAANC  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
45    continue
*    
        alpha = 1.d0
        beta  = 0.125d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
         
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
      return




cF46                         DIXMAANE  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
46    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return



cF47                       Partial Perturbed Quadratic                              
*                             
*                    Initial point x0=[0.5, 0.5, ..., 0.5].
*
47    continue
*    
        temp(1) = x(1) + x(2)
      do i=2,n-1
          temp(i) = temp(i-1) + x(i+1)
      end do              

      g(1)= 2.d0*x(1)
        do i=1,n-1
          g(1) = g(1) + temp(i)/50.d0
        end do
        
        do i=2,n
          g(i) = 2.d0*float(i)*x(i)
          do j=i,n          
            g(i) = g(i) + temp(j-1)/50.d0
          end do
        end do

      return


cF48                       Broyden Tridiagonal                              
*                             
*                  Initial point x0=[-1., -1., ..., -1.].
*
48    continue
*

      temp(1) = 3.d0*x(1) - 2.d0*x(1)*x(1)
      do i=2,n-1
        temp(i) = 3.d0*x(i)-2.d0*x(i)*x(i)-x(i-1)-2.d0*x(i+1)+1.d0
      end do
        
      temp(n) = 3.d0*x(n)-2.d0*x(n)*x(n)-x(n-1)+1.d0

      g(1) = 2.d0*temp(1)*(3.d0-4.d0*x(1)) - 2.d0*temp(2)
      
      g(2) = 2.d0*temp(2)*(3.d0-4.d0*x(2)) - 2.d0*temp(3)
      
      do i=3,n-1
        g(i) = -4.d0*temp(i-1) 
     *         +2.d0*temp(i)*(3.d0-4.d0*x(i)) 
     *         -2.d0*temp(i+1)             
      end do
      
      g(n) = -4.d0*temp(n-1) + 2.d0*temp(n)*(3.d0-4.d0*x(n))
      
      return
    


cF49                   Almost Perturbed Quadratic                              
*                             
*                  Initial point x0=[0.5, 0.5, ...,0.5].
*
49    continue
*    
      g(1) = 2.d0*x(1) + 0.02d0*(x(1)+x(n))
      
      do i=2,n-1
        g(i) = 2.d0*float(i)*x(i)
      end do
      
      g(n) = 2.d0*float(n)*x(n) + 0.02d0*(x(1)+x(n))
      
      return
      


cF50                   Tridiagonal Perturbed Quadratic                              
*                             
*                    Initial point x0=[0.5, 0.5, ...,0.5].
*
50    continue
*

      do i=1,n-2
        temp(i) = x(i) + x(i+1) + x(i+2)
      end do

      g(1) = 2.d0*x(1) + 2.d0*temp(1)
      
      g(2) = 4.d0*x(2) + 2.d0*(temp(1)+temp(2))
      
      do i=3,n-2
        g(i) = 2.d0*float(i)*x(i) + 2.d0*(temp(i-2)+temp(i-1)+temp(i))
      end do
      
      g(n-1) = 2.d0*float(n-1)*x(n-1) + 2.d0*(temp(n-3)+temp(n-2))
      
      g(n) =   2.d0*temp(n-2)
      
      return  


cF51                       EDENSCH Function  (CUTE)
*
*                      Initial Point: [0., 0., ..., 0.].
*-------------------------------------------------------------------
*

51    continue      
           
      g(1) = 4.d0*(x(1)-2.d0)**3 + 2.d0*x(2)*(x(1)*x(2)-2.d0*x(2))
*      
      do i=2,n-1               
        g(i) = 2.d0*(x(i-1)*x(i)-2.d0*x(i))*(x(i-1)-2.d0) +
     *         2.d0*(x(i)+1.d0) +
     *         4.d0*(x(i)-2.d0)**3 +
     *         2.d0*x(i+1)*(x(i)*x(i+1)-2.d0*x(i+1))   
      end do                                        
      
      g(n) = 2.d0*(x(n-1)*x(n)-2.d0*x(n))*(x(n-1)-2.d0) +
     *       2.d0*(x(n)+1.d0) 
      
      return
    
    
cF52                        VARDIM Function  (CUTE)
*
*                      Initial Point: [1-1/n, 1-2/n, ..., 1-n/n].
*-------------------------------------------------------------------
*

52    continue

      sum = 0.d0
      do i=1,n
        sum = sum + float(i)*x(i)
      end do
      
      sum = sum - float(n)*float(n+1)/2.d0

      do i=1,n
        g(i) = 2.d0*(x(i)-1.d0) +
     *         2.d0*sum*float(i) +
     *         4.d0*float(i)*(sum**3)   
      end do
      
      return
      


cF53                           STAIRCASE S1                               
*
*                     Initial point x0=[1,1,...,1].
*

53    continue

	  g(1) = 2.d0*(x(1)+x(2)-1.d0)
      do i=2,n-1
        g(i) = 2.d0*(x(i-1)+x(i)-float(i-1)) +  
     *         2.d0*(x(i)+x(i+1)-float(i))
      end do
      g(n) = 2.d0*(x(n-1)+x(n)-float(n-1))                     	  
      
      return





cF54                             LIARWHD (CUTE)                              
*                             
*                      Initial point x0=[4., 4., ....4.].
*

54    continue                  

      g(1) = 2.d0*(x(1)-1.d0) + 8.d0*(x(1)*x(1)-x(1))*(2.d0*x(1)-1.d0)
      do i=2,n
        g(1) = g(1) - 8.d0*(x(i)*x(i)-x(1))
      end do
      
      do i=2,n
        g(i) = 16.d0*x(i)*(x(i)*x(i)-x(1)) + 2.d0*(x(i)-1.d0)
      end do  
        
      return



cF55                        DIAGONAL 6                            
*                                
*                Initial point x0=[1.,1., ..., 1.].
*

55    continue   

      do i=1,n
        g(i) = dexp(x(i)) - 1.d0
      end do
                                                       
      return
       
       
cF56                       DIXON3DQ  (CUTE)
*
*                Initial Point x0=[-1, -1,..., -1]  March 7, 2005
*
56    continue

      g(1) = 2.d0*(x(1)-2.d0) + 2.d0*(x(1)-x(2))
      
      do i=2,n-1
        g(i) = -2.d0*(x(i-1)-x(i)) + 2.d0*(x(i)-x(i+1))
      end do
      
      g(n) = -2.d0*(x(n-1)-x(n)) + 2.d0*(x(n)-1.d0)
                
      return
      



cF57                        DIXMAANF (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
57    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0625d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return


cF58                         DIXMAANG  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
58    continue
*    
        alpha = 1.d0
        beta  = 0.125d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 1
        k3 = 0
        k4 = 1
           
        m = n/3
        
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return



cF59                         DIXMAANH  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
59    continue
*    
        alpha = 1.d0
        beta  = 0.26d0
        gamma = 0.26d0
        delta = 0.26d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF60                        DIXMAANI  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
60    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 2
           
        m = n/3
        
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF61                        DIXMAANJ  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
61    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF62                         DIXMAANK  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
62    continue
*    
        alpha = 1.d0
        beta  = 0.625d0
        gamma = 0.625d0
        delta = 0.625d0
                      
        k1 = 1
        k2 = 1
        k3 = 1
        k4 = 1
           
        m = n/3
       
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return





cF63                          DIXMAANL  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
63    continue
*    
        alpha = 1.d0
        beta  = 0.26d0
        gamma = 0.26d0
        delta = 0.26d0
                      
        k1 = 2
        k2 = 2
        k3 = 2
        k4 = 1
           
        m = n/3
        
        
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF64                         ENGVAL1 (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
64    continue                          
      
      do i=1,n-1
        t(i) = x(i)*x(i) + x(i+1)*x(i+1)
      end do
      
      
      g(1) = 4.d0*x(1)*t(1) - 4.d0
      
      do i=2,n-1
        g(i) = 4.d0*x(i)*t(i-1) + 4.d0*x(i)*t(i) - 4.d0
      end do
      
      g(n) = 4.d0*x(n)*t(n-1)  
      
      return      



cF65                         FLETCHCR (CUTE)
*
*                       Initial point: [0,0,...,0]
*
65    continue
      do i=1,n-1
        t(i) = x(i+1) - x(i) + 1.d0 - x(i)*x(i)
      end do

      g(1) = -200.d0 * (1.d0+2.d0*x(1)) * t(1)
      
      do i=2,n-1
        g(i) = 200.d0*t(i-1) - 200.d0*t(i)*(1.d0+2.d0*x(i))
      end do
      
      g(n) = 200.d0*t(n-1)
      
      return            




cF66                          COSINE (CUTE)
* 
*                      Initial point: [1,1,...,1]

66    continue

      g(1) = -(2.d0*x(1)) * dsin(x(1)**2 - x(2)/2.d0)
      
      do i=2,n-1
        g(i) = 0.5d0*dsin(x(i-1)**2 - x(i)/2.d0) - 
     *         (2.d0*x(i)) * dsin(x(i)**2 - x(i+1)/2.d0)
      end do
      
      g(n) = 0.5d0*dsin(x(n-1)**2 - x(n)/2.d0)
      
      return          



cF67                   DENSCHNB  (CUTE)
*
*               Initial point: [1,1,...,1]

67    continue

      j=1
      do i=1,n/2
        g(j) = 2.d0*(x(2*i-1)-2.d0) + 2.d0*(x(2*i-1)-2.d0)*x(2*i)*x(2*i)
        g(j+1) = ((x(2*i-1)-2.d0)**2)*2.d0*x(2*i) + 2.d0*(x(2*i)+1.d0)
        j=j+2
      end do
      
      return  


cF68                  DENSCHNF  (CUTE)
*
*               Initial point: [2,0,2,0,...,2,0]

68    continue

      j=1
      do i=1,n/2
      g(j)=2.d0*(2.d0*(x(2*i-1)+x(2*i))**2+(x(2*i-1)-x(2*i))**2-8.d0)*
     *  (4.d0*(x(2*i-1)+x(2*i))+2.d0*(x(2*i-1)-x(2*i))) +    
     *  2.d0*(5.d0*x(2*i-1)**2+(x(2*i)-3.d0)**2-9.d0)*10.d0*x(2*i-1)
      g(j+1)=2.d0*(2.d0*(x(2*i-1)+x(2*i))**2+(x(2*i-1)-x(2*i))**2-8.d0)*
     *  (2.d0*(x(2*i-1)+x(2*i))-2.d0*(x(2*i-1)-x(2*i))) +
     *  2.d0*(5.d0*x(2*i-1)**2+(x(2*i)-3.d0)**2-9.d0)*2.d0*(x(2*i)-3.d0)
          j=j+2
      end do           
      return
      
      
cF69                     SINQUAD (CUTE)
*                    
*             Initial Point: [0.1, 0.1, ..., 0.1]

69    continue

      g(1) = 4.d0*(x(1)-1.d0)**3 - 4.d0*x(1)*(x(n)**2-x(1)**2) 
      do i=1,n-2
        g(1) = g(1) - 4.d0*t(i)*x(1)
      end do
      
      do i=2,n-1
        g(i) = 2.d0*t(i-1)*(cos(x(i)-x(n))+2.d0*x(i))
      end do
      
      g(n) = 4.d0*x(n)*(x(n)**2-x(1)**2)
      do i=1,n-2
        g(n) = g(n) - 2.d0*t(i)*cos(x(i+1)-x(n))    
      end do
      return


cF70                     BIGGSB1 (CUTE)
*                    
*               Initial Point: [0., 0., ....,0.]

70    continue

      g(1) = 4.d0*x(1) - 2.d0*x(2) - 2.d0 
      
      do i=2,n-1
        g(i) = 4.d0*x(i) - 2.d0*x(i-1) - 2.d0*x(i+1)
      end do
      
      g(n) = 4.d0*x(n) - 2.d0*x(n-1) - 2.d0          
      
      return 

cF71                     Partial Perturbed Quadratic PPQ2
*
*                        Initial Point: [0.5,0.5,...,0.5] 

71	  continue

        temp(1) = x(1) + x(2)
        do i=2,n-1
          temp(i) = temp(i-1) + x(i+1)
        end do              

        g(1)= 2.d0*x(1)
        do i=1,n-1
          g(1) = g(1) + temp(i)/50.d0
        end do
        
        do i=2,n
          g(i) = 2.d0*x(i)/float(i)
          do j=i,n          
            g(i) = g(i) + temp(j-1)/50.d0
          end do
        end do         
 
      end         
c-------------------------------------------------------- End EVALG
      
      subroutine evalfg(n,x,f,g, nexp)
      real*8 x(n), f, g(n)  
      
      real*8 t1,t2,t3,t4, c, d
      real*8 s, temp(11000), temp1, tsum, sum
      real*8 u(11000), v(11000), t(11000) 
      real*8 u1, v1, c1, c2
      real*8 alpha, beta, gamma, delta
                       
      integer k1, k2, k3, k4      
                             
*      
      go to ( 1, 2, 3, 4, 5, 6, 7, 8, 9,10,
     *       11,12,13,14,15,16,17,18,19,20,
     *       21,22,23,24,25,26,27,28,29,30,
     *       31,32,33,34,35,36,37,38,39,40,
     *       41,42,43,44,45,46,47,48,49,50,
     *       51,52,53,54,55,56,57,58,59,60,
     *       61,62,63,64,65,66,67,68,69,70,
     *       71) nexp  
     
     

cF1                       Extended Freudenstein & Roth
*
*                      Initial Point: [0.5, -2, ...,0.5, -2].
*

1     continue
      f = 0.d0
      j=1         
      
      do i=1,n/2 
        t1=-13.d0+x(2*i-1)+5.d0*x(2*i)*x(2*i)-x(2*i)**3-2.d0*x(2*i) 
        t2=-29.d0+x(2*i-1)+x(2*i)**3+x(2*i)**2-14.d0*x(2*i)   
        
        f = f + t1*t1 + t2*t2
        
        g(j)  =2.d0*(t1+t2)   
        g(j+1)=2.d0*t1*(10.d0*x(2*i)-3.d0*x(2*i)*x(2*i)-2.d0) +
     *            2.d0*t2*(3.d0*x(2*i)*x(2*i)+2.d0*x(2*i)-14.d0)   
        j=j+2        
      end do   
      
      return

c********************************************************************
             


cF2                           Extended Trigonometric Function
*
*                          Initial Point: [0.2, 0.2, ....,0.2].

2     continue      
      s= float(n)
      do i=1,n
        s = s - dcos(x(i))
      end do   
      do i=1,n
        temp(i) = s + float(i)*(1.d0-dcos(x(i))) - dsin(x(i))
      end do 

      f = 0.d0
      do i=1,n
        f = f + temp(i)**2
      end do   
*--
      s=0.d0
      do i=1,n
        s = s + temp(i)
      end do
      
      do i=1,n
        g(i) = 2.d0*s*dsin(x(i)) + 
     +            2.d0*temp(i)*(float(i)*dsin(x(i))-dcos(x(i)))  
      end do      
      return
**************************************************************



cF3                    Extended Rosenbrock function
*
* Initial point: [-1.2, 1, -1.2, 1, ..........., -1.2, 1]      
      
3     continue                           
      c=100.d0                           
                           
      f=0.d0
      do i=1,n/2
        f = f + c*(x(2*i)-x(2*i-1)**2)**2 + (1.d0-x(2*i-1))**2
      end do  
*               
      j=1
      do i=1,n/2
        g(j)   = -4.d0*c*x(2*i-1)*(x(2*i)-x(2*i-1)**2) - 
     *            2.d0*(1.d0-x(2*i-1))
        g(j+1) =  2.d0*c*(x(2*i)-x(2*i-1)**2)
        j = j + 2
      end do
      
      return

c******************************************************************



cF4                    Extended White & Holst function
*
*           Initial point: [-1.2, 1, -1.2, 1, ..........., -1.2, 1]      
      
4     continue                           
      c=100.d0                           
                           
      f=0.d0
      do i=1,n/2
        f = f + c*(x(2*i)-x(2*i-1)**3)**2 + (1.d0-x(2*i-1))**2
      end do  
*               
      j=1
      do i=1,n/2
        g(j)   = -6.d0*c*x(2*i-1)*x(2*i-1)*(x(2*i)-x(2*i-1)**3) - 
     *            2.d0*(1.d0-x(2*i-1))
        g(j+1) =  2.d0*c*(x(2*i)-x(2*i-1)**3)
        j = j + 2
      end do
      
      return

c******************************************************************



*
cF5                  Extended Beale Function  U63 (MatrixRom)
*
*                   Initial Point: [1, 0.8, ...., 1, 0.8]
*--------------------------------------------------------------
*
5     continue
*      
      f=0.d0

      j=1      
      do i=1,n/2 
        t1=1.5d0  -x(2*i-1)+x(2*i-1)*x(2*i)
        t2=2.25d0 -x(2*i-1)+x(2*i-1)*x(2*i)*x(2*i)
        t3=2.625d0-x(2*i-1)+x(2*i-1)*x(2*i)*x(2*i)*x(2*i)   
        
        f = f + t1*t1 + t2*t2 + t3*t3
*------------------------------------------------------
        g(j)  =2.d0*t1*(-1.d0+x(2*i)) +
     *         2.d0*t2*(-1.d0+x(2*i)**2) +
     *         2.d0*t3*(-1.d0+x(2*i)**3)   
        g(j+1)=2.d0*t1*x(2*i-1) +
     *         2.d0*t2*2.d0*x(2*i-1)*x(2*i) +
     *         2.d0*t3*3.d0*x(2*i-1)*x(2*i)*x(2*i)   
        j=j+2        
      end do   
*   
      return



cF6                   Extended Penalty Function  U52 (MatrixRom)
*
*                           Intial Point: [1,2,3,.....,n].
*
6     continue      
      temp1=0.d0
      do i=1,n
        temp1 = temp1 + x(i)**2
      end do
*      
      f = (temp1 - 0.25d0)**2
      
      do i=1,n-1
        f = f + (x(i)-1.d0)**2 
      end do

*----------------------        

      do i=1,n-1
        g(i) = 2.d0*(x(i)-1.d0) + 4.d0*x(i)*(temp1-0.25d0)
      end do                                              
      
      g(n) = 4.d0*x(n)*(temp1-0.25d0)
      return


cF7                        Perturbed Quadratic function
*
*                     Initial Point:  [0.5, 0.5, ......, 0.5].
*     
7     continue
      
      temp1 = 0.d0
      do i=1,n
        temp1 = temp1 + x(i)
      end do  
      
      f = temp1*temp1/100.d0
      
      do i=1,n
        f = f + float(i)*x(i)**2      
      end do
*-----------------        
      do i=1,n
        g(i) = float(i) * 2.d0 * x(i) + temp1/50.d0
      end do        
      return



cF8                        Raydan 1 Function
*
*                   Initial point: [1, 1, ..., 1]

8     continue
      
      f=0.d0
      do i=1,n
        f = f + float(i) * (dexp(x(i))-x(i)) / 10.d0
      end do  
*-------------        
      do i=1,n
        g(i) = float(i) * (dexp(x(i)) - 1.d0) / 10.d0      
      end do  
      return




cF9                             Raydan 2 Function
*
*                       Initial Point: [1, 1, .....,1]
*  
9     continue
      
      f=0.d0
      do i=1,n
        f = f + dexp(x(i)) - x(i)
      end do
*--      
      do i=1,n
        g(i) = dexp(x(i)) - 1.d0
      end do
      return




cF10                             Diagonal1 Function
*
*                       Initial Point: [1/n, 1/n, ....., 1/n]
*  
10    continue
      
      f=0.d0
      
      do i=1,n
        f = f + dexp(x(i)) - x(i)*float(i)
      end do
*--      
      do i=1,n
        g(i) = dexp(x(i)) - float(i)
      end do
      return



cF11                             Diagonal2 Function
*
*                       Initial Point: [1/1, 1/2, ....., 1/n]
*  
11    continue
      
      f=0.d0
      
      do i=1,n
        f = f + dexp(x(i)) - x(i)/float(i)
      end do
*--      
      do i=1,n
        g(i) = dexp(x(i)) - 1.d0/float(i)
      end do
      return



cF12                         Diagonal3 Function
*
*                        Initial Point: [1,1,...,1] 

12    continue
      
      f=0.d0
      do i=1,n
        f = f + dexp(x(i)) - float(i)*dsin(x(i))
      end do
*--      
      do i=1,n
        g(i) = dexp(x(i)) - float(i)*dcos(x(i))
      end do
      return



cF13                          Hager Function
*
*                        Initial Point: [1,1,...,1] 


13    continue
      f=0.d0
      do i=1,n
        f = f + dexp(x(i)) - x(i)*sqrt(float(i))
      end do
*--      
      do i=1,n
        g(i) = dexp(x(i)) - sqrt(float(i))
      end do
      return



cF14                 Generalized Tridiagonal-1 Function
*
*                        Initial Point: [2,2,...,2] 


14    continue
      
      f=0.d0
      do i=1,n-1
        u(i) = x(i) + x(i+1) - 3.d0
        v(i) = x(i) - x(i+1) + 1.d0
      end do
      do i=1,n-1
        f = f + u(i)**2 + v(i)**4
      end do
*---                       
      g(1) = 2.d0*u(1) + 4.d0*v(1)**3
      do i=2,n-1
        g(i) = 2.d0*u(i-1) - 4.d0*v(i-1)**3 + 2.d0*u(i) + 4.d0*v(i)**3
      end do
      g(n) = 2.d0*u(n-1) - 4.d0*v(n-1)**3                         
      return



cF15                   Extended Tridiagonal-1 Function
*
*                        Initial Point: [2,2,...,2] 


15    continue
      
      f=0.d0
      do i=1,n/2
        u(i) = x(2*i-1) + x(2*i) - 3.d0
        v(i) = x(2*i-1) - x(2*i) + 1.d0
      end do
      do i=1,n/2
        f = f + u(i)**2 + v(i)**4
      end do
*--                       
      j=1
      do i=1,n/2
        g(j)   = 2.d0*u(i) + 4.d0*v(i)**3
        g(j+1) = 2.d0*u(i) - 4.d0*v(i)**3
        j=j+2
      end do                        
      return



cF16                        Extended Three Exponential Terms
*
*                          Intial Point: [0.1,0.1,.....,0.1].
*

16    continue
      f=0.d0   
      j=1
      do i=1,n/2
        t1=x(j) + 3.d0*x(j+1) - 0.1d0
        t2=x(j) - 3.d0*x(j+1) - 0.1d0
        t3=-x(j) - 0.1d0 
        f = f + dexp(t1) + dexp(t2) + dexp(t3)    
*--        
        g(j)   = dexp(t1) + dexp(t2) - dexp(t3)
        g(j+1) = 3.d0*dexp(t1) - 3.d0*dexp(t2)        
        j=j+2
      end do  
      return




cF17                       Generalized Tridiagonal-2  
*                      
*                    Initial point: [-1, -1, .........., -1., -1] 
 
17    continue
      f = 0.d0
      u(1) = 5.d0*x(1)-3.d0*x(1)**2-x(1)**3-3.d0*x(2)+1.d0
      
      do i=2,n-1
        u(i)=5.d0*x(i)-3.d0*x(i)**2-x(i)**3-x(i-1)-3.d0*x(i+1)+1.d0
      end do      
                
      u(n)=5.d0*x(n)-3.d0*x(n)**2-x(n)**3-x(n-1)+1.d0        
*      
      do i=1,n   
        f = f + u(i)**2
        v(i) = 5.d0 -6.d0*x(i) -3.d0*x(i)**2
      end do    
      
*----------------------------------------- Gradient

      g(1) = 2.d0*u(1)*v(1) - 2.d0*u(2)
      
      do i=2,n-1
        g(i) = -6.d0*u(i-1) + 2.d0*u(i)*v(i) -2.d0*u(i+1)
      end do
      
      g(n) = -6.d0*u(n-1) + 2.d0*u(n)*v(n)  
      return



cF18                       Diagonal4 Function  
*                      
*                  Initial point: [1, 1, .........., 1., 1] 
 
18    continue
      
      c=100.d0
      f = 0.d0
      do i=1,n/2
        f = f + (x(2*i-1)**2 + c*x(2*i)**2)/2.d0
      end do                  
*--
      j=1
      do i=1,n/2
        g(j)   = x(2*i-1)
        g(j+1) = c*x(2*i)
        j=j+2
      end do  
      return


cF19                       Diagonal5 Function  (MatrixRom)
*                      
*                    Initial point: [1.1, 1.1, .........., 1.1] 
 
19    continue
      
      f = 0.d0
      do i=1,n
        f = f + dlog( dexp(x(i)) + dexp(-x(i)) )
      end do                  
*--
      do i=1,n
        g(i) = (dexp(x(i)) - dexp(-x(i)))/(dexp(x(i)) + dexp(-x(i)))
      end do  
      return




cF20                         Extended Himmelblau Function
*
*                           Initial Point: [1, 1, ....., 1]

20    continue
      
      f=0.d0
      j=1
      do i=1,n/2
        u1 = x(2*i-1)**2 + x(2*i)    - 11.d0
        v1 = x(2*i-1)    + x(2*i)**2 - 7.d0 
        
        f = f + u1*u1 + v1*v1
*--
        g(j)   = 4.d0*u1*x(2*i-1) + 2.d0*v1
        g(j+1) = 2.d0*u1          + 4.d0*v1*x(2*i)
        j=j+2
      end do
      return




cF21                    Generalized PSC1 Function
*
*                   Initial point: [3, 0.1, ..., 3, 0.1]

21    continue

      f = 0.d0
      do i=1,n-1
        f = f + (x(i)**2 +x(i+1)**2 +x(i)*x(i+1))**2 
     *        + (dsin(x(i)))**2 + (dcos(x(i+1)))**2    
      end do

      g(1)=   2.d0*(x(1)**2+x(2)**2+x(1)*x(2))*(2.d0*x(1)+x(2)) 
     *       +2.d0 * dsin(x(1)) * dcos(x(1))
      
      do i=2,n-1
      g(i) =  2.d0*(x(i-1)**2+x(i)**2+x(i-1)*x(i))*(2.d0*x(i)+x(i-1))
     +       +2.d0*(x(i)**2+x(i+1)**2+x(i)*x(i+1))*(2.d0*x(i)+x(i+1))
      end do

      g(n) =  2.d0*(x(n-1)**2+x(n)**2+x(n-1)*x(n))*(2.d0*x(n)+x(n-1))
     +       -2.d0*dcos(x(n)) * dsin(x(n)) 
      return



cF22                       Extended PSC1 Function
*
*                   Initial point: [3, 0.1, ..., 3, 0.1]

22    continue 

      f = 0.d0
      do i=1,n/2
        f = f + (x(2*i-1)**2 +x(2*i)**2 +x(2*i-1)*x(2*i))**2 
     *        + (dsin(x(2*i-1)))**2 + (dcos(x(2*i)))**2    
      end do
*     
      j=1
      do i=1,n/2
      g(j)   = 2.d0*(x(2*i-1)**2+x(2*i)**2+x(2*i-1)*x(2*i)) *
     +              (2.d0*x(2*i-1)+x(2*i)) +
     +              2.d0*(dsin(x(2*i-1)))*(dcos(x(2*i-1)))
     
      g(j+1) = 2.d0*(x(2*i-1)**2+x(2*i)**2+x(2*i-1)*x(2*i)) *
     +              (2.d0*x(2*i)+x(2*i-1)) -
     +              2.d0*(dcos(x(2*i)))*(dsin(x(2*i)))
      j=j+2
      end do
      return




cF23                              Extended Powell 
*
*                      Initial Point: [3, -1, 0, 1, ......].
*-------------------------------------------------------------------
*
23    continue
      
      f=0.d0
      
      j=1    
      do i=1,n/4  
        t1= x(4*i-3) + 10.d0*x(4*i-2) 
        t2= x(4*i-1) - x(4*i)
        t3= x(4*i-2) - 2.d0*x(4*i-1)
        t4= x(4*i-3) - x(4*i)     
       
        f = f + t1*t1 + 5.d0*t2*t2 + t3**4 + 10.d0*t4**4      
*-        
        g(j)  =   2.d0*t1 + 40.d0*t4**3
        g(j+1)=  20.d0*t1 +  4.d0*t3**3
        g(j+2)=  10.d0*t2 -  8.d0*t3**3
        g(j+3)= -10.d0*t2 - 40.d0*t4**3
        
        j=j+4        
      end do               
      return



cF24                  Extended Block Diagonal BD1 Function 
*
*                     Initial Point: [0.1, 0.1, ..., 0.1].
*-------------------------------------------------------------------
*
24    continue
      
      f = 0.d0
      
      j=1
      do i=1,n/2
        t1 = x(2*i-1)**2 + x(2*i)**2 - 2.d0
        t2 = dexp(x(2*i-1)) - x(2*i)
        
        f = f + t1*t1 + t2*t2
        
        g(j)   = 4.d0*t1*x(2*i-1) + 2.d0*t2*dexp(x(2*i-1)-1.d0)
        g(j+1) = 4.d0*t1*x(2*i) - 2.d0*t2
        j=j+2
      end do
      return



cF25                           Extended Maratos Function 
*
*                      Initial Point: [1.1, 0.1, ...,1.1, 0.1].
*-------------------------------------------------------------------
*
25    continue
      
      c = 100.d0
      f = 0.d0
      
      j=1
      do i=1,n/2
        t1 = x(2*i-1)**2 + x(2*i)**2 - 1.d0
        
        f = f + (x(2*i-1) + c*t1*t1)
        
        g(j)   = 1.d0 + 4.d0 * c * t1 * x(2*i-1)
        g(j+1) =        4.d0 * c * t1 * x(2*i)
        j=j+2
      end do
      return




cF26                             Extended Cliff                  
*
*                     Initial Point:  [0, -1, ......, 0, -1].
*                     

26    continue
      
      f=0.d0
        
      j=1  
      do i=1,n/2
        temp1 = (x(2*i-1)-3.d0)/100.d0
        
      f = f+temp1*temp1-(x(2*i-1)-x(2*i))+dexp(20.d0*(x(2*i-1)-x(2*i)))
        
       g(j)   = temp1/50.d0 - 1.d0 + 20.d0*dexp(20.d0*(x(2*i-1)-x(2*i)))
       g(j+1) = 1.d0 - 20.d0*dexp(20.d0*(x(2*i-1)-x(2*i)))
       j=j+2
      end do
      
      return      




cF27                   Quadratic Diagonal Perturbed Function
*
*                     Initial Point:  [0.5, 0.5, ......, 0.5].
*     
27    continue
      
      temp1 = 0.d0
      do i=1,n
        temp1 = temp1 + x(i)
      end do  
      
      f = temp1*temp1
      
      do i=1,n
        f = f + (float(i)/100.d0) * x(i)**2      
      end do
*--     
      do i=1,n
        g(i) = float(i) * x(i) / 50.d0 + 2.d0*temp1
      end do        
      return



cF28                           Extended Wood Function
*
*                         Initial Point: [-3,-1,-3,-1,......]
*
28    continue
      
      f=0.d0
      
      j=1
      do i=1,n/4
        f = f + 100.d0*(x(4*i-3)**2-x(4*i-2))**2 
     *        +        (x(4*i-3)-1.d0)**2  
     *        +  90.d0*(x(4*i-1)**2-x(4*i))**2            
     *        +        (1.d0-x(4*i-1))**2
     *        +  10.1d0*(x(4*i-2)-1.d0)**2
     *        +  10.1d0*(x(4*i)  -1.d0)**2
     *        +  19.8d0*(x(4*i-2)-1.d0)*(x(4*i)-1.d0)
     
        g(j)   = 400.d0*(x(4*i-3)**2-x(4*i-2))*x(4*i-3)
     *             + 2.d0*(x(4*i-3)-1.d0)
        g(j+1) =-200.d0*(x(4*i-3)**2-x(4*i-2))
     *             + 20.2d0*(x(4*i-2)-1.d0)   
     *             + 19.8d0*(x(4*i)-1.d0)
        g(j+2) = 360.d0*(x(4*i-1)**2-x(4*i))*x(4*i-1)
     *             - 2.d0*(1.d0-x(4*i-1))   
        g(j+3) =-180.d0*(x(4*i-1)**2-x(4*i))
     *             + 20.2d0*(x(4*i)  -1.d0)
     *             + 19.8d0*(x(4*i-2)-1.d0)   
        j=j+4
      end do
      return




cF29                          Extended Hiebert Function
*
*                             Initial Point: [0,0,...0].

29    continue
      
      c1=10.d0
      c2=500.d0
      
      f = 0.d0
      
      j=1
      do i=1,n/2
        f = f + (x(2*i-1)-c1)**2 + (x(2*i-1)*x(2*i)-c2)**2

        g(j)   =   2.d0*(x(2*i-1)-c1) 
     *           + 2.d0*(x(2*i-1)*x(2*i)-c2)*x(2*i)       
        g(j+1) =   2.d0*(x(2*i-1)*x(2*i)-c2)*x(2*i-1)
        j=j+2
      end do
      return



cF30                           Quadratic Function QF1
*
*                             Initial Point: [1,1,...1].

30    continue
      
      f = 0.d0
      
      do i=1,n
        f = f + float(i)*x(i)*x(i)
        g(i)   =   float(i) * x(i)
      end do
      
      f = f/2.d0
      f = f - x(n)

      g(n) = g(n) - 1.d0
            
      return



cF31                Extended Quadratic Penalty QP1 Function           
*
*                     Initial Point:  [1, 1, ......,1].
*                     

31    continue
      
      t1=0.d0
      do i=1,n
        t1 = t1 + x(i)*x(i)
      end do
      t1 = t1 - 0.5d0  
                   
      f = 0.d0                   
      do i=1,n-1
        f = f + (x(i)*x(i) - 2.d0)**2
        g(i) = 4.d0*(x(i)*x(i)-2.d0)*x(i) + 4.d0*t1*x(i)
      end do  
      
      f = f + t1*t1
      g(n) = 4.d0*t1*x(n)
      
      return



cF32               Extended Quadratic Penalty QP2 Function           
*
*                     Initial Point:  [1, 1, ......,1].
*                     

32    continue
      
      t1=0.d0
      do i=1,n
        t1 = t1 + x(i)*x(i)
      end do
      t1 = t1 - 100.d0  
                   
      f = 0.d0                   
      do i=1,n-1
        f = f + (x(i)*x(i) - dsin(x(i)))**2
        g(i) = 2.d0*(x(i)*x(i)-dsin(x(i)))*(2.d0*x(i)-dcos(x(i))) 
     *       + 4.d0*t1*x(i)
      end do  
      
      f = f + t1*t1
      g(n) = 4.d0*t1*x(n)
      
      return




cF33                      A Quadratic Function QF2
*
*                     Initial Point:  [0.5, 0.5, ......,0.5].
*                     

33    continue
      
      f=0.d0
      do i=1,n 
        f = f + float(i)*(x(i)**2 - 1.d0)**2
      end do
      f = f/2.d0
      f = f - x(n)
*
      do i=1,n
        g(i) = float(i)*2.d0*x(i)*(x(i)**2 - 1.d0)
      end do
      g(n) = g(n) - 1.d0
      
      return



cF34                       Extended EP1 Function 
*
*                      Initial Point: [1.5.,1.5.,...,1.5].
*-------------------------------------------------------------------
*
34    continue
*      
      f=0.d0
      
      j=1                                     
      do i=1,n/2                                     
        t1=dexp(x(2*i-1)-x(2*i)) - 5.d0        
        t2=x(2*i-1)-x(2*i)
        t3=x(2*i-1)-x(2*i)-11.d0
        
        f = f + t1*t1 + (t2*t2)*(t3*t3)
      
        g(j) =   2.d0*t1*dexp(x(2*i-1)-x(2*i)) 
     *         + 2.d0*t2*t3*t3 
     *         + 2.d0*t2*t2*t3         
        g(j+1) = - g(j)
        j=j+2
      end do  
      
      return




cF35                  Extended Tridiagonal-2 Function 
*
*                      Initial Point: [1.,1.,...,1.].
*-------------------------------------------------------------------
*
35    continue
*      
      c=0.1d0
      f=0.d0
      
      do i=1,n-1                                    
        f = f + (x(i)*x(i+1)-1.d0)**2 + c*(x(i)+1.d0)*(x(i+1)+1.d0)
      end do
      
      g(1) = 2.d0*(x(1)*x(2)-1.d0)*x(2) + c*(x(2)+1.d0)
      
      do i=2,n-1
        g(i) = 2.d0*(x(i-1)*x(i)-1.d0)*x(i-1) + c*(x(i-1)+1.d0) +
     *         2.d0*(x(i)*x(i+1)-1.d0)*x(i+1) + c*(x(i+1)+1.d0)
      end do 
                                               
      g(n) = 2.d0*(x(n-1)*x(n)-1.d0)*x(n-1) + c*(x(n-1)+1.d0)

      return





cF36                           BDQRTIC (CUTE)
*
*                     Initial point x0=[1.,1.,...,1.].
*
*
36    continue
*        
      n4=n-4
      f=0.d0
        
      do i=1,n4
        temp(i) = x(i)**2 + 2.d0*x(i+1)**2 + 3.d0*x(i+2)**2 
     *                    + 4.d0*x(i+3)**2 + 5.d0*x(n)**2
      end do           
        
      do i=1,n4
        f = f + (-4.d0*x(i)+3.d0)**2 + temp(i)**2
      end do                                  
*
      g(1) = -8.d0*(-4.d0*x(1)+3.d0) + 
     *        (4.d0*temp(1))*x(1)        
      g(2) = -8.d0*(-4.d0*x(2)+3.d0) + 
     *        (8.d0*temp(1)+  4.d0*temp(2))*x(2)
      g(3) = -8.d0*(-4.d0*x(3)+3.d0) + 
     *        (12.d0*temp(1)+ 8.d0*temp(2)+ 4.d0*temp(3))*x(3)
      g(4) = -8.d0*(-4.d0*x(4)+3.d0) + 
     *        (16.d0*temp(1)+12.d0*temp(2) +8.d0*temp(3)+
     *          4.d0*temp(4))*x(4)
        
      do i=5,n4
        g(i) = -8.d0*(-4.d0*x(i)+3.d0) + 
     *         (16.d0*temp(i-3)+12.d0*temp(i-2)+
     *           8.d0*temp(i-1)+4.d0*temp(i))*x(i)                                                        
      end do
        
      g(n4+1) =(16.d0*temp(n4-2)+12.d0*temp(n4-1)+8.d0*temp(n4))*x(n4+1)  
      g(n4+2) =(16.d0*temp(n4-1)+12.d0*temp(n4))*x(n4+2)
      g(n4+3) =(16.d0*temp(n4))*x(n4+3)
          
      tsum=0.d0
      do i=1,n4
        tsum = tsum + temp(i)
      end do
      g(n) = 20.d0*tsum*x(n)
             
      return




cF37                           TRIDIA  (CUTE)                             
*
*                     Initial point x0=[1,1,...,1].
*
*
37    continue
*                
      alpha=2.d0
      beta=1.d0
      gamma=1.d0
      delta=1.d0
        
      f=gamma*(delta*x(1)-1.d0)**2
      
      do i=2,n
        f = f + float(i)*(alpha*x(i)-beta*x(i-1))**2
      end do
*---
      g(1) = 2.d0*gamma*(delta*x(1)-1.d0)*delta - 
     *       4.d0*(alpha*x(2)-beta*x(1))*beta
     
      do i=2,n-1
        g(i) = 2.d0*float(i)*(alpha*x(i)-beta*x(i-1))*alpha -
     *         2.d0*float(i+1)*(alpha*x(i+1)-beta*x(i))*beta
      end do         

      g(n) = 2.d0*float(n)*(alpha*x(n)-beta*x(n-1))*alpha

      return




cF38                       ARWHEAD  (CUTE)                             
*                          
*                     Initial point x0=[1,1,...,1].
*
*
38    continue

      f=0.d0
      do i=1,n-1
        f = f + (-4.d0*x(i)+3.d0) + (x(i)**2+x(n)**2)**2
      end do
      
      do i=1,n-1
        g(i) = -4.d0 + 4.d0*x(i)*(x(i)**2+x(n)**2)
      end do
      
      g(n) = 0.d0
      do i=1,n-1
        g(n) = g(n) + 4.d0*x(n)*(x(i)**2+x(n)**2)
      end do                    
      
      return




cF39                                                         
*                       NONDIA (Shanno-78)  (CUTE)
*
*                     Initial point x0=[-1,-1,...,-1].
*
*
39    continue

      c=100.d0
      
      f=(x(1)-1.d0)**2 + c*(x(1)-x(1)**2)**2
      
      do i=2,n
        f = f + c*(x(1)-x(i)**2)**2
      end do
*--
      g(1)=2.d0*(x(1)-1.d0) + 2.d0*c*(x(1)-x(1)**2)*(1.d0-2.d0*x(1))        
      do i=2,n
        g(1) = g(1) + 2.d0*c*(x(1)-x(i)**2)
      end do  
      
      do i=2,n
        g(i) = -4.d0*c*x(i)*(x(1)-x(i)**2)
      end do        
              
      return



cF40                          NONDQUAR  (CUTE)                             
*                             
*                     Initial point x0=[1,-1,1,-1,...,].
*
*
40    continue

      f = (x(1)-x(2))**2 + (x(n-1)+x(n))**2      
      
      do i=1,n-2
        f = f + (x(i)+x(i+1)+x(n))**4
      end do
*--
      g(1) = 2.d0*(x(1)-x(2))+4.d0*(x(1)+x(2)+x(n))**3
      g(2) =-2.d0*(x(1)-x(2))+4.d0*(x(1)+x(2)+x(n))**3 +
     *                        4.d0*(x(2)+x(3)+x(n))**3
      
      do i=3,n-2
        g(i) = 4.d0*(x(i-1)+x(i)+x(n))**3 +
     *         4.d0*(x(i)+x(i+1)+x(n))**3
      end do          
      
      g(n-1) = 4.d0*(x(n-2)+x(n-1)+x(n))**3 +
     *         2.d0*(x(n-1)+x(n))
      
      g(n) = 2.d0*(x(n-1)+x(n))
      do i=1,n-2
        g(n) = g(n) + 4.d0*(x(i)+x(i+1)+x(n))**3
      end do  
              
      return



cF41                          DQDRTIC                              
*                             
*                     Initial point x0=[3,3,3...,3].
*
*
41    continue
      
      c=100.d0
      d=100.d0
      
      f=0.d0
      do i=1,n-2
        f = f + (x(i)**2 + c*x(i+1)**2 + d*x(i+2)**2)
      end do
*--
      g(1) = 2.d0*x(1)
      g(2) = 2.d0*c*x(2) + 2.d0*x(2)
      
      do i=3,n-2
        g(i) = 2.d0*(1.d0+d+c)*x(i)
      end do
      
      g(n-1) = 2.d0*(c+d)*x(n-1)          
      g(n) = 2.d0*d*x(n)
      
      return
       



cF42                          EG2 (CUTE)                             
*                             
*                     Initial point x0=[1,1,1...,1].
*
*
42    continue

      f=0.5d0*dsin(x(n)*x(n))
      do i=1,n-1
        f = f + dsin(x(1)+x(i)*x(i)-1.d0)
      end do
*--
      g(1)=(1.d0+2.d0*x(1))*dcos(x(1)+x(1)*x(1)-1.d0)
      do i=2,n-1
        g(1) = g(1) + dcos(x(1)+x(i)*x(i)-1.d0)
      end do               
      
      do i=2,n-1
        g(i) = 2.d0*x(i)*dcos(x(1)+x(i)*x(i)-1.d0)
      end do
      
      g(n) = x(n)*dcos(x(n)*x(n))

      return


cF43                        DIXMAANA (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
43    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return
                                        



cF44                         DIXMAANB (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
44    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0625d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
      return
                                        



cF45                        DIXMAANC  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
45    continue
*    
        alpha = 1.d0
        beta  = 0.125d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 0
        k2 = 0
        k3 = 0
        k4 = 0
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
      return




cF46                         DIXMAANE  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
46    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return



cF47                       Partial Perturbed Quadratic                              
*                             
*                    Initial point x0=[0.5, 0.5, ..., 0.5].
*
47    continue
*    
        temp(1) = x(1) + x(2)
        do i=2,n-1
          temp(i) = temp(i-1) + x(i+1)
        end do              
        
        f=x(1)*x(1)
        do i=2,n
          f = f + float(i)*x(i)*x(i) + temp(i-1)*temp(i-1)/100.d0
        end do
*
        g(1)= 2.d0*x(1)
        do i=1,n-1
          g(1) = g(1) + temp(i)/50.d0
        end do
        
        do i=2,n
          g(i) = 2.d0*float(i)*x(i)
          do j=i,n          
            g(i) = g(i) + temp(j-1)/50.d0
          end do
        end do

      return


cF48                       Broyden Tridiagonal                              
*                             
*                  Initial point x0=[-1., -1., ..., -1.].
*
48    continue
*

      temp(1) = 3.d0*x(1) - 2.d0*x(1)*x(1)
      do i=2,n-1
        temp(i) = 3.d0*x(i)-2.d0*x(i)*x(i)-x(i-1)-2.d0*x(i+1)+1.d0
      end do
        
      temp(n) = 3.d0*x(n)-2.d0*x(n)*x(n)-x(n-1)+1.d0
      
      f = 0.d0
      
      do i=1,n
        f = f + temp(i)*temp(i)
      end do
*----
      g(1) = 2.d0*temp(1)*(3.d0-4.d0*x(1)) - 2.d0*temp(2)
      
      g(2) = 2.d0*temp(2)*(3.d0-4.d0*x(2)) - 2.d0*temp(3)
      
      do i=3,n-1
        g(i) = -4.d0*temp(i-1) 
     *         +2.d0*temp(i)*(3.d0-4.d0*x(i)) 
     *         -2.d0*temp(i+1)             
      end do
      
      g(n) = -4.d0*temp(n-1) + 2.d0*temp(n)*(3.d0-4.d0*x(n))
      
      return
    


cF49                   Almost Perturbed Quadratic                              
*                             
*                  Initial point x0=[0.5, 0.5, ...,0.5].
*
49    continue
*    

      f = 0.01d0*(x(1)+x(n))**2
      
      do i=1,n
        f = f + float(i)*x(i)*x(i)
      end do     
*-
      g(1) = 2.d0*x(1) + 0.02d0*(x(1)+x(n))
      
      do i=2,n-1
        g(i) = 2.d0*float(i)*x(i)
      end do
      
      g(n) = 2.d0*float(n)*x(n) + 0.02d0*(x(1)+x(n))
      
      return
      


cF50                   Tridiagonal Perturbed Quadratic                              
*                             
*                    Initial point x0=[0.5, 0.5, ...,0.5].
*
50    continue
*

      do i=1,n-2
        temp(i) = x(i) + x(i+1) + x(i+2)
      end do
      
      f = x(1)*x(1)
      do i=2,n-1
        f = f + float(i)*x(i)*x(i) + temp(i-1)**2
      end do    
*--
      g(1) = 2.d0*x(1) + 2.d0*temp(1)
      
      g(2) = 4.d0*x(2) + 2.d0*(temp(1)+temp(2))
      
      do i=3,n-2
        g(i) = 2.d0*float(i)*x(i) + 2.d0*(temp(i-2)+temp(i-1)+temp(i))
      end do
      
      g(n-1) = 2.d0*float(n-1)*x(n-1) + 2.d0*(temp(n-3)+temp(n-2))
      
      g(n) =   2.d0*temp(n-2)
      
      return  


cF51                       EDENSCH Function  (CUTE)
*
*                      Initial Point: [0., 0., ..., 0.].
*-------------------------------------------------------------------
*

51    continue      

      f = 16.d0

      do i=1,n-1
        f = f + (x(i)-2.d0)**4 + 
     *          (x(i)*x(i+1)-2.d0*x(i+1))**2 +
     *          (x(i+1)+1.d0)**2
      end do
          
*---------------------------------------------------------
           
      g(1) = 4.d0*(x(1)-2.d0)**3 + 2.d0*x(2)*(x(1)*x(2)-2.d0*x(2))
*      
      do i=2,n-1               
        g(i) = 2.d0*(x(i-1)*x(i)-2.d0*x(i))*(x(i-1)-2.d0) +
     *         2.d0*(x(i)+1.d0) +
     *         4.d0*(x(i)-2.d0)**3 +
     *         2.d0*x(i+1)*(x(i)*x(i+1)-2.d0*x(i+1))   
      end do                                        
      
      g(n) = 2.d0*(x(n-1)*x(n)-2.d0*x(n))*(x(n-1)-2.d0) +
     *       2.d0*(x(n)+1.d0) 
      
      return
    
    
cF52                        VARDIM Function  (CUTE)
*
*                      Initial Point: [1-1/n, 1-2/n, ..., 1-n/n].
*-------------------------------------------------------------------
*

52    continue

      sum = 0.d0
      do i=1,n
        sum = sum + float(i)*x(i)
      end do
      
      sum = sum - float(n)*float(n+1)/2.d0
      
      f = sum**2 + sum**4
      do i=1,n               
        f = f + (x(i)-1.d0)**2
      end do  
*--
      do i=1,n
        g(i) = 2.d0*(x(i)-1.d0) +
     *         2.d0*sum*float(i) +
     *         4.d0*float(i)*(sum**3)   
      end do
      
      return
      


cF53                           STAIRCASE S1                               
*
*                     Initial point x0=[1,1,...,1].
*

53    continue

	  f=0.d0
      do i=1,n-1
        f = f + (x(i)+x(i+1)-float(i))**2
      end do
c
	  g(1) = 2.d0*(x(1)+x(2)-1.d0)
      do i=2,n-1
        g(i) = 2.d0*(x(i-1)+x(i)-float(i-1)) +  
     *         2.d0*(x(i)+x(i+1)-float(i))
      end do
      g(n) = 2.d0*(x(n-1)+x(n)-float(n-1))                     	  
      
      return





cF54                             LIARWHD (CUTE)                              
*                             
*                      Initial point x0=[4., 4., ....4.].
*

54    continue                  

      f=0.d0
      do i=1,n
        f = f + 4.d0*(x(i)*x(i) - x(1))**2 + (x(i)-1.d0)**2
      end do  

*--
      g(1) = 2.d0*(x(1)-1.d0) + 8.d0*(x(1)*x(1)-x(1))*(2.d0*x(1)-1.d0)
      do i=2,n
        g(1) = g(1) - 8.d0*(x(i)*x(i)-x(1))
      end do
      
      do i=2,n
        g(i) = 16.d0*x(i)*(x(i)*x(i)-x(1)) + 2.d0*(x(i)-1.d0)
      end do  
        
      return



cF55                        DIAGONAL 6                            
*                                
*                Initial point x0=[1.,1., ..., 1.].
*

55    continue   
        
      f = 0.d0
      
      do i=1,n
        f = f + dexp(x(i)) - (1.d0+x(i))
      end do                         
*-      
      do i=1,n
        g(i) = dexp(x(i)) - 1.d0
      end do
                                                       
      return
       
       
cF56                       DIXON3DQ  (CUTE)
*
*                Initial Point x0=[-1, -1,..., -1]  March 7, 2005
*
56    continue

      f=(x(1)-2.d0)**2
      
      do i=1,n-1
        f = f + (x(i)-x(i+1))**2
      end do
      
      f = f + (x(n)-1.d0)**2
c
      g(1) = 2.d0*(x(1)-2.d0) + 2.d0*(x(1)-x(2))
      
      do i=2,n-1
        g(i) = -2.d0*(x(i-1)-x(i)) + 2.d0*(x(i)-x(i+1))
      end do
      
      g(n) = -2.d0*(x(n-1)-x(n)) + 2.d0*(x(n)-1.d0)
                
      return
      



cF57                        DIXMAANF (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
57    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0625d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return


cF58                         DIXMAANG  (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
58    continue
*    
        alpha = 1.d0
        beta  = 0.125d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 1
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return



cF59                         DIXMAANH  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
59    continue
*    
        alpha = 1.d0
        beta  = 0.26d0
        gamma = 0.26d0
        delta = 0.26d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF60                        DIXMAANI  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
60    continue
*    
        alpha = 1.d0
        beta  = 0.d0
        gamma = 0.125d0
        delta = 0.125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 2
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF61                        DIXMAANJ  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
61    continue
*    
        alpha = 1.d0
        beta  = 0.0625d0
        gamma = 0.0625d0
        delta = 0.0125d0
                      
        k1 = 1
        k2 = 0
        k3 = 0
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF62                         DIXMAANK  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
62    continue
*    
        alpha = 1.d0
        beta  = 0.625d0
        gamma = 0.625d0
        delta = 0.625d0
                      
        k1 = 1
        k2 = 1
        k3 = 1
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return





cF63                          DIXMAANL  (CUTE)                             
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
63    continue
*    
        alpha = 1.d0
        beta  = 0.26d0
        gamma = 0.26d0
        delta = 0.26d0
                      
        k1 = 2
        k2 = 2
        k3 = 2
        k4 = 1
           
        m = n/3
        
        f = 1.d0
        do i=1,n
          f = f + alpha * x(i)* x(i) * (float(i)/float(n))**k1
        end do
        
        do i=1,n-1
          f = f + beta*x(i)*x(i)*(x(i+1)+x(i+1)*x(i+1)) *
     *            (float(i)/float(n))**k2
        end do
        
        do i=1,2*m
          f = f + gamma * x(i)*x(i) * (x(i+m)**4) *
     *            (float(i)/float(n))**k3
        end do
        
        do i=1,m
          f = f + delta * x(i) * x(i+2*m) *
     *            (float(i)/float(n))**k4
        end do
*--     
        g(1) = 2.d0*alpha*x(1)*(float(1)/float(n))**k1 +
     *         2.d0*beta*x(1)*(x(2)+x(2)**2)**2 * 
     *            (float(1)/float(n))**k2  +
     *         2.d0*gamma*x(1)*x(1+m)**4 * (float(1)/float(n))**k3 +
     *              delta*x(1+2*m)*(float(1)/float(n))**k4
        do i=2,m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *                delta*x(i+2*m) * (float(i)/float(n))**k4
        end do
        
        do i=m+1,2*m
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +                                    
     *           2.d0*gamma*x(i)*x(i+m)**4 * (float(i)/float(n))**k3 +
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3
        end do
        
        do i=2*m+1,n-1
          g(i) = 2.d0*alpha*x(i)*(float(i)/float(n))**k1 +
     *           2.d0*beta*x(i-1)**2 *(x(i)+x(i)*x(i))*(1.d0+2.d0*x(i))*
     *                (float(i-1)/float(n))**k2 +
     *           2.d0*beta*x(i)*(x(i+1)+x(i+1)**2)**2 *
     *                (float(i)/float(n))**k2 +  
     *           4.d0*gamma*x(i-m)**2 * x(i)**3 * 
     *                (float(i-m)/float(n))**k3 +
     *                delta*x(i-2*m) * (float(i-2*m)/float(n))**k4
        end do        
        
        g(n) = 2.d0*alpha*x(n) + 
     *         2.d0*beta*x(n-1)**2 * (x(n)+x(n)**2)*(1.d0+2.d0*x(n)) *
     *                 (float(n-1)/float(n))**k2 +
     *         4.d0*gamma*x(2*m)**2 * x(n)**3 * 
     *              (float(2*m)/float(n))**k3 +
     *              delta*x(m) * (float(m)/float(n))**k4
     
        return




cF64                         ENGVAL1 (CUTE)                            
*                             
*                    Initial point x0=[2.,2.,2...,2.].
*
64    continue                          
      
      do i=1,n-1
        t(i) = x(i)*x(i) + x(i+1)*x(i+1)
      end do
      
      f = 0.d0
      
      do i=1,n-1
        f = f + t(i)*t(i) + (-4.d0*x(i) + 3.d0)
      end do   
c
      g(1) = 4.d0*x(1)*t(1) - 4.d0
      
      do i=2,n-1
        g(i) = 4.d0*x(i)*t(i-1) + 4.d0*x(i)*t(i) - 4.d0
      end do
      
      g(n) = 4.d0*x(n)*t(n-1)  
      
      return      



cF65                         FLETCHCR (CUTE)
*
*                       Initial point: [0,0,...,0]
*
65    continue

      f=0.d0    
      
      do i=1,n-1
        t(i) = x(i+1) - x(i) + 1.d0 - x(i)*x(i)
      end do
      
      do i=1,n-1
        f = f + 100.d0*t(i)*t(i)
      end do
c
      g(1) = -200.d0 * (1.d0+2.d0*x(1)) * t(1)
      
      do i=2,n-1
        g(i) = 200.d0*t(i-1) - 200.d0*t(i)*(1.d0+2.d0*x(i))
      end do
      
      g(n) = 200.d0*t(n-1)
      
      return            




cF66                          COSINE (CUTE)
* 
*                      Initial point: [1,1,...,1]

66    continue

      f=0.d0
      do i=1,n-1
        f = f + dcos(x(i)*x(i) - x(i+1)/2.d0)
      end do
c
      g(1) = -(2.d0*x(1)) * dsin(x(1)**2 - x(2)/2.d0)
      
      do i=2,n-1
        g(i) = 0.5d0*dsin(x(i-1)**2 - x(i)/2.d0) - 
     *         (2.d0*x(i)) * dsin(x(i)**2 - x(i+1)/2.d0)
      end do
      
      g(n) = 0.5d0*dsin(x(n-1)**2 - x(n)/2.d0)
      
      return          



cF67                   DENSCHNB  (CUTE)
*
*               Initial point: [1,1,...,1]

67    continue

      f=0.d0
      do i=1,n/2
        f = f + (x(2*i-1)-2.d0)**2 +
     *          ((x(2*i-1)-2.d0)**2)*(x(2*i)**2) +
     *          (x(2*i)+1.d0)**2
      end do   
c       
      j=1
      do i=1,n/2
        g(j) = 2.d0*(x(2*i-1)-2.d0) + 2.d0*(x(2*i-1)-2.d0)*x(2*i)*x(2*i)
        g(j+1) = ((x(2*i-1)-2.d0)**2)*2.d0*x(2*i) + 2.d0*(x(2*i)+1.d0)
        j=j+2
      end do
      
      return  


cF68                  DENSCHNF  (CUTE)
*
*               Initial point: [2,0,2,0,...,2,0]

68    continue

      f=0.d0
      do  i=1,n/2
        f=f+(2.d0*(x(2*i-1)+x(2*i))**2+(x(2*i-1)-x(2*i))**2-8.d0)**2+
     *      (5.d0*x(2*i-1)**2+(x(2*i)-3.d0)**2-9.d0)**2
      end do
c
      j=1
      do i=1,n/2
      g(j)=2.d0*(2.d0*(x(2*i-1)+x(2*i))**2+(x(2*i-1)-x(2*i))**2-8.d0)*
     *  (4.d0*(x(2*i-1)+x(2*i))+2.d0*(x(2*i-1)-x(2*i))) +    
     *  2.d0*(5.d0*x(2*i-1)**2+(x(2*i)-3.d0)**2-9.d0)*10.d0*x(2*i-1)
      g(j+1)=2.d0*(2.d0*(x(2*i-1)+x(2*i))**2+(x(2*i-1)-x(2*i))**2-8.d0)*
     *  (2.d0*(x(2*i-1)+x(2*i))-2.d0*(x(2*i-1)-x(2*i))) +
     *  2.d0*(5.d0*x(2*i-1)**2+(x(2*i)-3.d0)**2-9.d0)*2.d0*(x(2*i)-3.d0)      
        j=j+2
      end do           
      return
      
      
cF69                     SINQUAD (CUTE)
*                    
*             Initial Point: [0.1, 0.1, ..., 0.1]

69    continue

      f=(x(1)-1.d0)**4 + (x(n)**2-x(1)**2)**2
      
      do i=1,n-2
        t(i) = sin(x(i+1)-x(n)) - x(1)**2 + x(i+1)**2
        f = f + t(i)*t(i)
      end do
c      
      g(1) = 4.d0*(x(1)-1.d0)**3 - 4.d0*x(1)*(x(n)**2-x(1)**2) 
      do i=1,n-2
        g(1) = g(1) - 4.d0*t(i)*x(1)
      end do
      
      do i=2,n-1
        g(i) = 2.d0*t(i-1)*(cos(x(i)-x(n))+2.d0*x(i))
      end do
      
      g(n) = 4.d0*x(n)*(x(n)**2-x(1)**2)
      do i=1,n-2
        g(n) = g(n) - 2.d0*t(i)*cos(x(i+1)-x(n))    
      end do
      return


cF70                     BIGGSB1 (CUTE)
*                    
*               Initial Point: [0., 0., ....,0.]

70    continue
      
      f=(x(1)-1.d0)**2 + (1.d0-x(n))**2
      do i=2,n
        f = f + (x(i)-x(i-1))**2
      end do
c
      g(1) = 4.d0*x(1) - 2.d0*x(2) - 2.d0 
      
      do i=2,n-1
        g(i) = 4.d0*x(i) - 2.d0*x(i-1) - 2.d0*x(i+1)
      end do
      
      g(n) = 4.d0*x(n) - 2.d0*x(n-1) - 2.d0          
      
      return 

cF71                     Partial Perturbed Quadratic PPQ2
*
*                        Initial Point: [0.5,0.5,...,0.5] 

71	  continue

        temp(1) = x(1) + x(2)
        do i=2,n-1
          temp(i) = temp(i-1) + x(i+1)
        end do              
        
        f=x(1)*x(1)
        do i=2,n
          f = f + x(i)*x(i)/float(i) + temp(i-1)*temp(i-1)/100.d0
        end do
*
        g(1)= 2.d0*x(1)
        do i=1,n-1
          g(1) = g(1) + temp(i)/50.d0
        end do
        
        do i=2,n
          g(i) = 2.d0*x(i)/float(i)
          do j=i,n          
            g(i) = g(i) + temp(j-1)/50.d0
          end do
        end do         
 
      end         
c-------------------------------------------------------- End EVALFG      