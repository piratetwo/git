"""
Student template code for Project 3
Student will implement five functions:

slow_closest_pair(cluster_list)
fast_closest_pair(cluster_list)
closest_pair_strip(cluster_list, horiz_center, half_width)
hierarchical_clustering(cluster_list, num_clusters)
kmeans_clustering(cluster_list, num_clusters, num_iterations)

where cluster_list is a 2D list of clusters in the plane
"""

import math
import random
import alg_cluster
import time
import matplotlib.pyplot as plt



######################################################
# Code for closest pairs of clusters

def pair_distance(cluster_list, idx1, idx2):
    """
    Helper function that computes Euclidean distance between two clusters in a list

    Input: cluster_list is list of clusters, idx1 and idx2 are integer indices for two clusters
    
    Output: tuple (dist, idx1, idx2) where dist is distance between
    cluster_list[idx1] and cluster_list[idx2]
    """
    return (cluster_list[idx1].distance(cluster_list[idx2]), min(idx1, idx2), max(idx1, idx2))


def slow_closest_pair(cluster_list):
    """
    Compute the distance between the closest pair of clusters in a list (slow)

    Input: cluster_list is the list of clusters
    
    Output: tuple of the form (dist, idx1, idx2) where the centers of the clusters
    cluster_list[idx1] and cluster_list[idx2] have minimum distance dist.       
    """
    dist = float("Inf")
    idx1 = -1
    idx2 = -1
    size = len(cluster_list)
    for idx in xrange(size-1):
        for jdx in xrange(idx+1,size):
                temp = cluster_list[idx].distance(cluster_list[jdx])
                if temp < dist:
                    dist = temp
                    idx1 = idx
                    idx2 = jdx
    return (dist,min(idx1,idx2),max(idx1,idx2))



def fast_closest_pair(cluster_list):
    """
    Compute the distance between the closest pair of clusters in a list (fast)

    Input: cluster_list is list of clusters SORTED such that horizontal positions of their
    centers are in ascending order
    
    Output: tuple of the form (dist, idx1, idx2) where the centers of the clusters
    cluster_list[idx1] and cluster_list[idx2] have minimum distance dist.       
    """
    
    dist = float("Inf")
    idx1 = -1
    idx2 = -1

    size = len(cluster_list)
    if size <= 3:
        return slow_closest_pair(cluster_list)
    else:
        mmm = int(math.floor(size / 2))
        left = cluster_list[0:mmm]
        right = cluster_list[mmm:size]
        (dist1,i11,j11) = fast_closest_pair(left)
        bbb = fast_closest_pair(right)
        if dist1 < bbb[0]:
            dist = dist1
            idx1 = i11
            idx2 = j11
        else:
            dist = bbb[0]
            idx1 = bbb[1] + mmm
            idx2 = bbb[2] + mmm

        mid = (left[-1].horiz_center() + right[0].horiz_center()) / 2
        aaa = closest_pair_strip(cluster_list, mid, dist)
        if aaa[0] < dist:
            dist = aaa[0]
            idx1 = aaa[1]
            idx2 = aaa[2]
    return (dist,min(idx1,idx2),max(idx1,idx2))


def closest_pair_strip(cluster_list, horiz_center, half_width):
    """
    Helper function to compute the closest pair of clusters in a vertical strip
    
    Input: cluster_list is a list of clusters produced by fast_closest_pair
    horiz_center is the horizontal position of the strip's vertical center line
    half_width is the half the width of the strip (i.e; the maximum horizontal distance
    that a cluster can lie from the center line)

    Output: tuple of the form (dist, idx1, idx2) where the centers of the clusters
    cluster_list[idx1] and cluster_list[idx2] lie in the strip and have minimum distance dist.       
    """
    mid = [idx for idx in xrange(len(cluster_list)) 
           if abs(cluster_list[idx].horiz_center() 
                  - horiz_center) < half_width]
    mid.sort(key = lambda idx: cluster_list[idx].vert_center())
    num = len(mid)
    result = (float("inf"), -1, -1)
    for idx1 in xrange(num - 1):
        for idx2 in xrange(idx1 + 1, min(idx1 + 4, num)):
            current_d = pair_distance(cluster_list, mid[idx1], mid[idx2])
            if current_d < result:
                result = current_d
    if result[1] > result[2]:
        result = (result[0], result[2], result[1])
    return result
            
 
    
######################################################################
# Code for hierarchical clustering


def hierarchical_clustering(cluster_list, num_clusters):
    """
    Compute a hierarchical clustering of a set of clusters
    Note: the function may mutate cluster_list
    
    Input: List of clusters, integer number of clusters
    Output: List of clusters whose length is num_clusters
    """
    num = len(cluster_list)
    while num > num_clusters:	
        cluster_list.sort(key = lambda clu: clu.horiz_center())
        idx = fast_closest_pair(cluster_list)
        cluster_list[idx[1]].merge_clusters(cluster_list[idx[2]])
        cluster_list.pop(idx[2])
        num -= 1
    return cluster_list


######################################################################
# Code for k-means clustering

    
def kmeans_clustering(cluster_list, num_clusters, num_iterations):
    """
    Compute the k-means clustering of a set of clusters
    Note: the function may not mutate cluster_list
    
    Input: List of clusters, integers number of clusters and number of iterations
    Output: List of clusters whose length is num_clusters
    """

    # position initial clusters at the location of clusters with largest populations
    def center_distance(cluster_center, center_point):
        """
        input:
            cluster_center: a tuple
            center_point: a tuple
        output:
            the Euclidean distance between the two points
        """
        vert_dist = cluster_center[1] - center_point[1]
        horiz_dist = cluster_center[0] - center_point[0]
        return math.sqrt(vert_dist ** 2 + horiz_dist ** 2)
        
    # initialize k-means clusters to be initial clusters with largest populations
    pop_and_index = [(cluster_list[idx].total_population(), idx) 
                        for idx in range(len(cluster_list))]  
    pop_and_index.sort()
    pop_order = [pop_and_index[idx][1] for idx in range(len(pop_and_index))]
    centers = [(cluster_list[idx].horiz_center(), cluster_list[idx].vert_center()) 
               for idx in pop_order[:-(num_clusters+1):-1]]
    for dummy_iteration in range(num_iterations):
        kmeans_clusters = []
        for idx in range(num_clusters):
            kmeans_clusters.append(alg_cluster.Cluster(set([]), centers[idx][0], 
                                                       centers[idx][1], 0, 0.0))
        
        for cluster in cluster_list:
            cluster_center = (cluster.horiz_center(), cluster.vert_center())
            dists = [center_distance(cluster_center, center) for center in centers]
            min_idx = dists.index(min(dists))
            kmeans_clusters[min_idx].merge_clusters(cluster)
        
        centers = [(k_cluster.horiz_center(), k_cluster.vert_center()) for k_cluster
                   in kmeans_clusters]
    
    return kmeans_clusters

def gen_random_clusters(num_clusters):
	lis = []
	for item in xrange(num_clusters):
		ddx = random.uniform(-1,1)
		ddy = random.uniform(-1,1)
		lis.append(alg_cluster.Cluster(set([]),ddx,ddy,0,0.0))
	return lis

def Question_1():
	sizes = xrange(2,101)
	time1 = []
	time2 = []
	for item in sizes:
		test = gen_random_clusters(item)
		start = time.time()
		slow_closest_pair(test)
		time1.append(time.time()-start)
	
		start = time.time()
		fast_closest_pair(test)
		time2.append(time.time()-start)

	plt.plot(sizes,time1,'r-',label='slow_closest_pair')
	plt.plot(sizes,time2,'b-',label='fast_closest_pair')
	plt.title('Comparison of running times by Desktop Python')
	plt.xlabel('Number of initial clusters')
	plt.ylabel('Running time in seconds')
	plt.legend(loc='upper left')
	plt.show()














