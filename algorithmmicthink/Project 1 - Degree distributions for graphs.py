# general imports
import urllib2
import math
import simpleplot
import random
# import codeskulptor
# codeskulptor.set_timeout(200)
"""
Project 1 - Degree distributions for graphs
"""
EX_GRAPH0 = {0:set([1,2]),
             1:set([]),
             2:set([])}
             
EX_GRAPH1 = {0:set([1,4,5]),
             1:set([2,6]),
             2:set([3]),
             3:set([0]),
             4:set([1]),
             5:set([2]),
             6:set([])}
             
EX_GRAPH2 = {0:set([1,4,5]),
             1:set([2,6]),
             2:set([3,7]),
             3:set([7]),
             4:set([1]),
             5:set([2]),
             6:set([]),
             7:set([3]),
             8:set([1,2]),
             9:set([0,3,4,5,6,7])}

# in-degrees distribution of Application1 : 27770 papers
results = {0: 4594, 1: 3787, 2: 2699, 3: 1994, 4: 1638, 5: 1327, 6: 1138, 7: 901, 8: 824, 9: 690, 
    10: 591, 11: 527, 12: 483, 13: 447, 14: 409, 15: 322, 16: 293, 17: 274, 18: 278, 19: 250, 20: 222,
    21: 185, 22: 185, 23: 162, 24: 160, 25: 136, 26: 128, 27: 126, 28: 138, 29: 121, 30: 125, 31: 88,
    32: 100, 33: 88, 34: 68, 35: 86, 36: 66, 37: 82, 38: 75, 39: 66, 40: 52, 41: 72, 42: 53, 43: 69, 
    44: 50, 45: 56, 46: 52, 47: 44, 48: 29, 49: 48, 50: 34, 51: 33, 52: 32, 53: 31, 54: 33, 55: 29,
    56: 35, 57: 29, 58: 27, 59: 29, 60: 25, 61: 25, 62: 28, 63: 23, 64: 26, 65: 23, 66: 21, 67: 23,
    68: 24, 69: 19, 70: 22, 71: 21, 72: 10, 73: 17, 74: 13, 75: 19, 76: 18, 77: 15, 78: 8, 79: 24, 
    80: 9, 81: 12, 82: 15, 83: 8, 84: 11, 85: 14, 86: 7, 87: 13, 88: 10, 89: 14, 90: 6, 91: 7, 
    92: 10, 93: 5, 94: 17, 95: 10, 96: 10, 97: 10, 98: 4, 99: 9, 100: 7, 101: 9, 102: 9, 103: 4, 
    104: 6, 105: 8, 106: 10, 107: 8, 108: 6, 109: 8, 110: 5, 111: 5, 112: 3, 113: 9, 114: 8, 
    115: 5, 116: 6, 117: 3, 118: 8, 119: 5, 120: 2, 121: 5, 122: 3, 123: 4, 124: 6, 125: 5, 126: 5, 
    127: 2, 129: 5, 130: 1, 131: 4, 132: 1, 133: 6, 134: 3, 135: 1, 136: 6, 137: 3, 138: 3, 139: 4, 
    140: 1, 141: 4, 142: 6, 143: 3, 144: 5, 145: 3, 146: 3, 147: 1, 148: 6, 149: 4, 150: 4, 
    151: 5, 152: 2, 153: 3, 154: 4, 155: 4, 156: 2, 157: 4, 158: 3, 159: 5, 160: 1, 295: 1, 
    162: 2, 164: 3, 165: 1, 167: 2, 168: 1, 169: 2, 171: 2, 172: 6, 173: 2, 174: 2, 1199: 1, 
    176: 2, 177: 2, 178: 2, 179: 2, 180: 1, 181: 1, 182: 1, 183: 1, 184: 1, 185: 1, 186: 3, 
    187: 1, 188: 2, 701: 1, 190: 3, 191: 2, 192: 2, 193: 2, 194: 2, 196: 3, 197: 2, 198: 1,
    199: 1, 201: 3, 204: 3, 205: 4, 208: 2, 1144: 1, 211: 1, 212: 1, 213: 1, 214: 1, 217: 1, 
    219: 1, 220: 2,222: 2, 223: 4, 224: 1, 225: 1, 228: 2, 229: 3, 230: 2, 232: 3, 233: 2,
    235: 1, 748: 1, 1775: 1, 240: 1, 242: 2, 244: 1, 189: 1, 247: 2, 251: 1, 252: 1, 
    1299: 1, 257: 2, 520: 1, 775: 1, 264: 1, 265: 1, 268: 1, 273: 1, 274: 1, 1155: 1, 
    788:2, 1032: 1, 282: 2, 290: 1, 807: 1, 297: 1, 301: 3, 304: 1, 308: 1, 1641: 1, 
    315: 1, 651: 1, 325: 2, 327: 2, 328: 2, 329: 2, 331: 1, 333: 1, 337: 1, 340: 1, 
    341: 1, 344: 1, 347: 1, 314: 1, 2414: 1, 373: 1, 380: 2, 383: 1, 385: 1, 388: 1, 
    1114: 1, 1006: 1, 406: 1, 411: 1, 421: 1, 424: 1, 426: 1, 427: 1, 438: 1, 456: 1, 
    175: 1, 467: 1, 475: 1, 494: 1}

def make_complete_graph(num_nodes):
    """
    Takes the number of nodes num_nodes and returns
    a dictionary corresponding to a complete directed 
    graph with the specified number of nodes
    """
    nodes = dict()
    if num_nodes != 0:		
        for num in range(num_nodes):
            list1 = range(num_nodes)
            list1.remove(num)
            nodes[num] = set(list1)
             
    return nodes

#print make_complete_graph(7)
    
def compute_in_degrees(digraph):
    """
    Takes a directed graph digraph (represented as a dictionary)
    and computes the in-degrees for the nodes in the graph
    """
    nodes = dict()
    if digraph != {}:
        values = digraph.values()
        for value in values:
            for item in value:
                nodes[item] = nodes.get(item,0) + 1
        
        for item in digraph.keys():
            if item not in nodes.keys():
                nodes[item] = 0
    
    return nodes
              
#print compute_in_degrees(EX_GRAPH2)

def in_degree_distribution(digraph):
    """
    Takes a directed graph digraph (represented as a dictionary)
    and computes the unnormalized distribution of the in-degrees of the graph.
    """
    nodes = dict()
    degrees = compute_in_degrees(digraph)
    for value in degrees.values():
        nodes[value] = nodes.get(value,0) + 1
            
    return nodes

#print in_degree_distribution(EX_GRAPH0)

def plot(diagraph):
    """
    plot a diagraph
    """
    values = diagraph.values()
    summ = float(sum(values))
    logdiag = dict()
    for key,value in diagraph.items():
        if key != 0:
            key = math.log(key)
        value = math.log(value / summ)
        logdiag[key] = value
        
    simpleplot.plot_lines('A normalized distribution on a log/log scale', 400, 300,
                          'In degrees', 'Number of Nodes', [logdiag], True)

def ER(n,p):
    """
    generate a random directed graph
    """
    graph = {}
    
    for i in range(0,n):
        list = []
        for j in range(0,n):
            if j != i:
                a = random.random()
                if a < p:
                    list.append(j)
        graph[i] = set(list)
        
    return graph

"""
Provided code for Application portion of Module 1

Imports physics citation graph 
"""
def load_graph(graph_url):
    """
    Function that loads a graph given the URL
    for a text representation of the graph
    
    Returns a dictionary that models a graph
    """
    graph_file = urllib2.urlopen(graph_url)
    graph_text = graph_file.read()
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[ : -1]
    
    print "Loaded graph with", len(graph_lines), "nodes"
    
    answer_graph = {}
    for line in graph_lines:
        neighbors = line.split(' ')
        node = int(neighbors[0])
        answer_graph[node] = set([])
        for neighbor in neighbors[1 : -1]:
            answer_graph[node].add(int(neighbor))

    return answer_graph

"""
Provided code for application portion of module 1

Helper class for implementing efficient version
of DPA algorithm
"""
class DPATrial:
    """
    Simple class to encapsulate optimized trials for DPA algorithm
    
    Maintains a list of node numbers with multiple instances of each number.
    The number of instances of each node number are
    in the same proportion as the desired probabilities
    
    Uses random.choice() to select a node number from this list for each trial.
    """

    def __init__(self, num_nodes):
        """
        Initialize a DPATrial object corresponding to a 
        complete graph with num_nodes nodes
        
        Note the initial list of node numbers has num_nodes copies of
        each node number
        """
        self._num_nodes = num_nodes
        self._node_numbers = [node for node in range(num_nodes) for dummy_idx in range(num_nodes)]


    def run_trial(self, num_nodes):
        """
        Conduct num_node trials using by applying random.choice()
        to the list of node numbers
        
        Updates the list of node numbers so that the number of instances of
        each node number is in the same ratio as the desired probabilities
        
        Returns:
        Set of nodes
        """
        
        # compute the neighbors for the newly-created node
        new_node_neighbors = set()
        for dummy_idx in range(num_nodes):
            new_node_neighbors.add(random.choice(self._node_numbers))
        
        # update the list of node numbers so that each node number 
        # appears in the correct ratio
        self._node_numbers.append(self._num_nodes)
        self._node_numbers.extend(list(new_node_neighbors))
        
        #update the number of nodes
        self._num_nodes += 1
        return new_node_neighbors
    

# Question 1
#CITATION_URL = "http://storage.googleapis.com/codeskulptor-alg/alg_phys-cite.txt"
#citation_graph = load_graph(CITATION_URL)
#in_degrees = in_degree_distribution(EX_GRAPH2)
#plot(results)

# Question 2
#graph = ER(100,0.8)
#in_degrees = in_degree_distribution(graph)
#plot(in_degrees)

# Question 3
# Compute the average out degree of application 1

# sum = 0
# for key,value in results.items():
	# sum += key * value
	
# print float(sum) / 27770
m = 13
n = 27770

# Question 4
# Question 4


def	dpa(n, m):
	"""
	implemention of DPA algorithm
	"""
	graph = make_complete_graph(m)
	DPA = DPATrial(m)
	for i in range (m, n-1):
		graph[i] = DPA.run_trial(m)
	return graph

#complete = dpa(n,m)
#distribution = in_degree_distribution(complete)
#print distribution
# results of DPA
distribution = {2560: 1, 513: 1, 2: 2289, 3: 1316, 4: 962, 5: 658, 6: 507, 1: 4615, 8: 310, 9: 282, 10: 187, 523: 2, 12: 155, 13: 134,14: 110, 8207: 1, 16: 81, 17: 78, 18: 69, 19: 60, 20: 43, 21: 57, 22: 53, 23: 48, 24: 42, 25: 45, 26: 32, 27: 33, 28: 33, 90: 5, 30: 17, 31: 30, 32: 23, 33: 22, 34: 22, 35: 14, 36: 22, 37: 15, 38: 14, 39: 17, 40: 14, 41: 13, 42: 15, 43: 13, 44: 10, 45: 12, 46: 13, 93: 4, 48: 15, 49: 10, 50: 10, 1075: 1, 52: 9, 94: 4, 566: 1, 55: 6, 56: 8, 180: 1, 58: 5, 59:6, 60: 10, 61: 11, 62: 10, 63: 8, 64: 4, 65: 3, 66: 4, 67: 9, 68: 4, 69: 4, 70: 5, 71: 9, 72: 7, 73: 8, 74: 2, 75: 7, 76: 7, 866: 1, 78: 3, 591: 1, 80: 2, 81: 4, 594: 1, 83: 9, 596: 1, 122: 2, 86: 5, 599: 1, 88: 6, 89: 7, 1114: 1, 15: 100,92: 4, 7773: 1, 1118: 1, 95: 5, 96: 3, 97: 3, 98: 7, 611: 1, 100: 6, 101: 2, 102: 1, 103: 3, 104: 1, 105: 2, 106: 3, 619: 1, 108: 3, 109: 3, 1646: 1, 111: 1, 113: 2, 114: 3, 7796: 1, 190: 1, 118: 1, 3191: 1, 120: 3, 121: 4, 634: 1, 2683: 1, 124: 1, 125: 5, 126: 3, 8319: 1, 128: 1, 2240: 1, 131: 2, 2181: 1, 134: 1, 8327: 1, 136: 1, 137: 1, 138: 2, 139: 2, 140: 4, 141: 2, 142: 2, 143: 1, 144: 2, 4753: 1, 146: 3, 147: 1, 660: 1, 149: 3, 150: 2, 151: 2, 152: 3, 153: 3, 154: 1, 0: 13937, 157: 1, 158: 1, 159: 2, 193: 1, 162: 3, 163: 1, 164: 2, 165: 1, 166: 3, 167: 1, 169: 1, 1194: 2, 171: 1, 172: 1, 173: 1, 686: 1, 175: 1, 1201: 1, 178: 1, 179: 1, 2740: 1, 2229: 1, 184: 1, 116: 1, 186: 1, 188: 1, 192: 2, 702: 1, 117: 2, 704: 2, 8385: 1, 194: 2, 199: 1, 200: 1, 201: 1, 119: 1, 206: 3, 207: 2, 208: 2, 210: 1, 211: 2, 212: 2, 215: 1, 7:396, 804: 1, 219: 1, 733: 1, 222: 1, 223: 1, 8928: 1, 225: 2, 226: 1, 123: 2, 228: 1, 231: 1, 232: 1, 234: 1, 1774: 1, 239: 2, 2292: 1, 246: 1, 248: 1, 127: 3, 253: 2, 255: 1, 257: 1, 258: 2, 259: 1, 260: 1, 261: 1, 262: 1, 129: 2, 2825: 1, 267: 1, 269: 1, 270: 2, 1295: 1, 272: 1, 787: 1, 276: 1, 277: 2, 280: 1, 283: 1, 2844: 1, 285: 3, 286: 1, 133: 2, 288:1, 87: 6, 5412: 1, 453: 1, 811: 1, 300: 1, 6706: 1, 302: 1, 1245: 1, 305: 1, 53: 10, 51: 11, 7479: 1, 564: 1, 827: 1, 319: 1, 320: 1, 321: 1, 1349: 1, 328: 2, 329: 1, 79: 5, 333: 1, 11: 169, 1361: 1, 2387: 1, 341: 1, 57: 8, 349: 2, 1374: 1, 400: 1, 354: 1, 355: 1, 145: 3, 1385: 1, 29: 31, 367: 1, 370: 1, 3956: 1, 373: 1, 375: 1, 376: 1, 148: 2, 1914: 1, 91:2, 381: 1, 107: 4, 384: 1, 900: 1, 901: 1, 902: 1, 47: 12, 291: 1, 1420: 1, 911: 1, 1936: 1, 238: 1, 406: 1, 920: 1, 412: 1, 414: 1, 99: 1, 156: 1, 427: 1, 428: 1, 431: 1, 244: 1, 448: 1, 160: 1, 965: 1, 454: 1, 54: 6, 458: 1, 1485: 1, 462: 2, 77: 3, 466: 1, 473: 1, 475: 1, 478: 1, 8160: 1, 997: 1, 486: 1, 1002: 1, 491: 1, 492: 1, 82: 3, 1518: 1, 495: 1, 1011: 1, 500: 2, 8181: 1, 8182: 1, 84: 3, 509: 1, 85: 3, 8191: 1}


