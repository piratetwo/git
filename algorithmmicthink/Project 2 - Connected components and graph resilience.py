#!/usr/bin/env python
# Project 2 - Connected components and graph resilience 
import random
from collections import deque
import matplotlib.pyplot as plt
import time

"""
Provided code for application portion of module 2

Helper class for implementing efficient version
of UPA algorithm
"""
class UPATrial:
    """
    Simple class to encapsulate optimizated trials for the UPA algorithm
    
    Maintains a list of node numbers with multiple instance of each number.
    The number of instances of each node number are
    in the same proportion as the desired probabilities
    
    Uses random.choice() to select a node number from this list for each trial.
    """

    def __init__(self, num_nodes):
        """
        Initialize a UPATrial object corresponding to a 
        complete graph with num_nodes nodes
        
        Note the initial list of node numbers has num_nodes copies of
        each node number
        """
        self._num_nodes = num_nodes
        self._node_numbers = [node for node in range(num_nodes) for dummy_idx in range(num_nodes)]


    def run_trial(self, num_nodes):
        """
        Conduct num_nodes trials using by applying random.choice()
        to the list of node numbers
        
        Updates the list of node numbers so that each node number
        appears in correct ratio
        
        Returns:
        Set of nodes
        """
        
        # compute the neighbors for the newly-created node
        new_node_neighbors = set()
        for _ in range(num_nodes):
            new_node_neighbors.add(random.choice(self._node_numbers))
        
        # update the list of node numbers so that each node number 
        # appears in the correct ratio
        self._node_numbers.append(self._num_nodes)
        for dummy_idx in range(len(new_node_neighbors)):
            self._node_numbers.append(self._num_nodes)
        self._node_numbers.extend(list(new_node_neighbors))
        
        #update the number of nodes
        self._num_nodes += 1
        return new_node_neighbors


def load_graph():
	answer_graph = {}
	graph_file = open('alg_rf7.txt','r')
	graph_text = graph_file.read()
	graph_lines = graph_text.split('\n')
	graph_lines = graph_lines[ : -1]
	for line in graph_lines:
		neighbors = line.split(' ')
		node = int(neighbors[0])
		answer_graph[node] = set([])
		for neighbor in neighbors[1 : -1]:
			answer_graph[node].add(int(neighbor))
	return answer_graph


############################################
# Provided code

def copy_graph(graph):
    """
    Make a copy of a graph
    """
    new_graph = {}
    for node in graph:
        new_graph[node] = set(graph[node])
    return new_graph

def delete_node(ugraph, node):
    """
    Delete a node from an undirected graph
    """
    neighbors = ugraph[node]
    ugraph.pop(node)
    for neighbor in neighbors:
        ugraph[neighbor].remove(node)
    
def targeted_order(ugraph):
    """
    Compute a targeted attack order consisting
    of nodes of maximal degree
    
    Returns:
    A list of nodes
    """
    # copy the graph
    new_graph = copy_graph(ugraph)
    
    order = []    
    while len(new_graph) > 0:
        max_degree = -1
        for node in new_graph:
            if len(new_graph[node]) > max_degree:
                max_degree = len(new_graph[node])
                max_degree_node = node
        
        neighbors = new_graph[max_degree_node]
        new_graph.pop(max_degree_node)
        for neighbor in neighbors:
            new_graph[neighbor].remove(max_degree_node)

        order.append(max_degree_node)
    return order

def ER(n,p):
	"""
	generate random undirected graph
	"""
	graph = {}
	for node in range(n):
		graph[node]=set([])
	#edges = 0
	for nodei in range(n):
		for nodej in xrange(nodei+1,n):
			a = random.random()
			if a < p:
				graph[nodei].add(nodej)
				graph[nodej].add(nodei)
	return graph

#print ER(10,0.3)

def make_complete_graph(num_nodes):
    """
    Takes the number of nodes num_nodes and returns
    a dictionary corresponding to a complete directed 
    graph with the specified number of nodes
    """
    nodes = dict()
    if num_nodes != 0:		
        for num in range(num_nodes):
            list1 = range(num_nodes)
            list1.remove(num)
            nodes[num] = set(list1)
             
    return nodes


"""
project 2
"""
import random
from collections import deque
def bfs_visited(ugraph, start_node):
    """
     Takes the undirected graph ugraph and the node start_node and
     returns the set consisting of all nodes that are visited
     by a breadth-first search that starts at start_node
    """
    que = deque()
    visited=[]
    visited.append(start_node)
    que.append(start_node)
    while len(que) != 0:
        out = que.popleft()
        for item in ugraph[out]:
            if item not in visited:
                visited.append(item)
                que.append(item)
    return set(visited)

def cc_visited(ugraph):
    """
    Takes the undirected graph ugraph and returns a list of 
    sets, where each set consists of all the nodes (and 
    nothing else) in a connected component, and there 
    is exactly one set in the list for each connected 
    component in ugraph and nothing else.
    """
    remaining_nodes = ugraph.keys()
    connected_comp_list = []
    while len(remaining_nodes) != 0:
        working_set = bfs_visited(ugraph, remaining_nodes[0])
        connected_comp_list.append(working_set)
        for node in working_set:
            remaining_nodes.remove(node)
    return connected_comp_list
        
def largest_cc_size(ugraph):
    """
    Takes the undirected graph ugraph and returns 
    the size (an integer) of the largest connected component in ugraph.
    """
    larg = 0
    for item in cc_visited(ugraph):
        length = len(item)
        if larg < length:
            larg = length
    return larg

def compute_resilience(ugraph, attack_order):
    """
    Takes the undirected graph ugraph, a list of nodes 
    attack_order and iterates through the nodes in 
    attack_order. 
    """
    resilience_list = []
    size = largest_cc_size(ugraph)
    resilience_list.append(size)
    for attacked_node in attack_order:
        ugraph.pop(attacked_node)
        for dummy_node in ugraph:
            if attacked_node in ugraph[dummy_node]:
                ugraph[dummy_node].remove(attacked_node)
        size = largest_cc_size(ugraph)
        resilience_list.append(size)
    return resilience_list

def upa(n, m):
	"""
	implemention of DPA algorithm
	"""
	graph = make_complete_graph(m)
	UPA = UPATrial(m)
	for i in range (m, n):
		temp = UPA.run_trial(m)
		graph[i] = temp
		for item in temp:
			graph[item].add(i)
	return graph
#attack_order=[1,2,0]
#print bfs_visited(ugraph, 1)
#print cc_visited(ugraph)
#print largest_cc_size(ugraph)
#print compute_resilience(ugraph, attack_order)

edges = 3047
nodes = 1239
# Question 1
p = 0.00397
print float(edges*2/nodes/(nodes-1))
print 1238*1239/2* 0.00397292620945

def random_order(graph):
	"""
	takes a graph and returns a list of the nodes in the graph in some random order
	"""
	items = graph.keys()
	return random.sample(items,len(items))
hor = xrange(1,1241)
graph = load_graph()
ran = ER(nodes,p)
up = upa(nodes,3)
graph1=copy_graph(graph)
ran1=copy_graph(ran)
up1=copy_graph(up)

attack_order1 = random_order(graph)
real = compute_resilience(graph, attack_order1)
plt.plot(hor, real, 'r--',label='NETWORK')


attack_order2 = random_order(ran)
err = compute_resilience(ran, attack_order2)
plt.plot(hor, err, 'b-.',label='ER,p=0.00397')


attack_order3 = random_order(up)
upr = compute_resilience(up, attack_order3)
plt.plot(hor, upr, 'g-',label='UPA,m=3')

plt.xlabel('the number of nodes removed')
plt.ylabel('the size of the largest connect component in the graphs')
plt.title('network vs er vs upa')
plt.legend()
#plt.show()

check_pt = int(1240 * 0.2)
resil_20_pct = 0.75 * 1239 * 0.8
print('resilience check value = ' + str(resil_20_pct))
print('network graph @20% = ' + str(real[check_pt]))
print('er graph @20% = ' + str(err[check_pt]))
print('upa graph @20% = ' + str(upr[check_pt]))


# Question 4
attack_order11 = targeted_order(graph1)
real1 = compute_resilience(graph1, attack_order11)
plt.plot(hor, real1, 'r--',label='NETWORK')


attack_order22 = targeted_order(ran1)
err1 = compute_resilience(ran1, attack_order22)
plt.plot(hor, err1, 'b-.',label='ER,p=0.00397')


attack_order33 = targeted_order(up1)
upr1 = compute_resilience(up1, attack_order33)
plt.plot(hor, upr1, 'g-',label='UPA,m=3')

plt.xlabel('the number of nodes removed')
plt.ylabel('the size of the largest connect component in the graphs')
plt.title('network vs er vs upa')
plt.legend()
plt.show()

check_pt = int(1240 * 0.2)
resil_20_pct = 0.75 * 1239 * 0.8
print('resilience check value = ' + str(resil_20_pct))
print('network graph @20% = ' + str(real1[check_pt]))
print('er graph @20% = ' + str(err1[check_pt]))
print('upa graph @20% = ' + str(upr1[check_pt]))

# Question 3

def fast_targeted_order(ugraph):
	"""
	fast degree computation
	"""
	size = len(ugraph)
	Degreesets = {}
	for k in range(size):
		Degreesets[k] = set([])
	for i in range(size):
		d = len(ugraph[i])
		Degreesets[d].add(i)
	L=[]
	i=0
	for k in range(size-1,-1,-1):
		while len(Degreesets[k])!=0:
			u=random.choice(list(Degreesets[k]))
			Degreesets[k].remove(u)
			for node in ugraph[u]:
				d=len(ugraph[node])
				Degreesets[d].remove(node)
				Degreesets[d-1].add(node)
			L.insert(i,u)
			i+=1
			
			delete_node(ugraph,u)
	return L

m=5
sizes=[]
t1=[]
t2=[]
for n in range(10,1000,10):
	sizes.append(n)
	ugraph = upa(n,5)
	start = time.time()
	targeted_order(ugraph)
	t1.append(time.time()-start)
	start = time.time()
	fast_targeted_order(ugraph)
	t2.append(time.time()-start)
plt.plot(sizes,t1,'r-',label='targeted_order')
plt.plot(sizes,t2,'b--',label='fast_targeted_order')
plt.title('Comparison of running times by Desktop Python')
plt.xlabel('Number of Nodes in upa graph')
plt.ylabel('Running time in seconds')
plt.legend(loc='upper left')
plt.show()



