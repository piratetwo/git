import math
# given an integer n, isprime(n) return whether n is a prime
def isprime(n):
	if n==1:
		return False
	elif n<4:
		return True
	elif n%2==0:
		return False
	elif n<9:
		return True
	elif n%3==0:
		return False
	else:
		r=int(math.sqrt(n))
		f =5
		while f<=r:
			if n%f==0:
				return False
			if n%(f+2)==0:
				return False
			f=f+6
		return True

limit = 10001
count =1
a=1
d=[]
while count !=limit:
	a=a+2
	if isprime(a):
		d.append(a)
		count+=1
print a
		
