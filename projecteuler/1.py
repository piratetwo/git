def summ(m,n):
	"""
	compute the sum of all number which can be divided by m under n
	"""
	num = n//m
	su = num * (num + 1) / 2
	return su * m

n=1000-1
print summ(3,n)+summ(5,n)-summ(15,n)
