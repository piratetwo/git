def leap(year):
	if year%100==0:
		if year%400==0:
			return True
	elif year % 4==0:
		return True
	else:
		return False

days=0
for year in xrange(1900,2001):
	if leap(year):
		days+=366
	else:
		days+=365

print days//7
