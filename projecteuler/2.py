limit = 4000000
summ=0
a=1
b=1
while b<limit:
	if b%2==0:
		summ+=b
	h=a+b
	b=a
	a=h
print summ

# second method
a=2
b=8
summ=a+b
c=4*b+a
while c<limit:
	summ+=c
	a=b
	b=c
	c=4*b+a
print summ
